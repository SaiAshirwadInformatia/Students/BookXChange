/*--------------USERS TABLE----------------*/

drop table if exists users;

-- --------------------------------

CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fname` varchar(128) NOT NULL,
 `lname` varchar(128) NOT NULL,
 `username` varchar(128) NOT NULL,
 `email` varchar(64) NOT NULL,
 `password` varchar(256) NOT NULL,
 `phone` varchar(16) NOT NULL,
 `dob` date NOT NULL,
 `blood_group` varchar(8) DEFAULT NULL,
 `gender` varchar(8) NOT NULL,
 `display_picture` text,
 `access_token` varchar(256) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) NOT NULL DEFAULT '0',
 `is_public` int(1) NOT NULL DEFAULT '1',
 PRIMARY KEY (`id`),
 UNIQUE KEY `email` (`email`),
 UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Default users data
--

INSERT INTO `users` (`fname`, `lname`, `username`, `email`, `password`, `phone`, `gender`, `dob`, `access_token`, `display_picture`) VALUES
('Akshay', 'Mane', 'akshaym', 'mane.akshay1997@gmail.com', 'cd84d683cc5612c69efe115c80d0b7dc', '9619249577', 'm', '1997-01-04', '8e2ee7610e08e053063cee084229d109', 'upload/72ee7d79ab4bd510ffa14f53c8c706ef'),
('Kunal', 'Band', 'kunalb', 'kunalbund1996@gmail.com', 'cd84d683cc5612c69efe115c80d0b7dc', '9820761848', 'm', '1996-09-25', '40cd64dd08c7e49c0bbab7a57d415030', 'upload/f8409e5c21ceff54755b325d9c2389dd'),
('Chaitali', 'Patil', 'cpatil', 'cpatil@saiashirwad.com', 'cd84d683cc5612c69efe115c80d0b7dc', '', 'f', '', '41cd64dd08c7e49c0bbab7a57d415031', 'upload/aea943a0e5f2c8aa784832e848ab0001');


/*---------------BOOKS TABLE----------------------*/

drop table if exists books;

-- ------------------------------------
	
CREATE TABLE `books` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `description` text NOT NULL,
 `author` varchar(128) NOT NULL,
 `published` varchar(128) NOT NULL,
 `published_year` date DEFAULT NULL,
 `price` int(7) NOT NULL,
 `user_id` int(11) NOT NULL,
 `genre_id` int(11) NOT NULL,
 `pages` int(11) DEFAULT NULL,
 `isbn10` varchar(32) DEFAULT NULL,
 `isbn13` varchar(32) DEFAULT NULL,
 `language` varchar(64) NOT NULL,
 `cover_picture` text DEFAULT NULL,
 `is_approved` int(1) DEFAULT '0',
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) NOT NULL DEFAULT '1',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- Dumping data for table `books`
--

INSERT INTO `books` ( `name`, `description`, `author`, `published`, `published_year`, `price`, `user_id`, `genre_id`, `pages`, `isbn10`, `isbn13`, `language`, `cover_picture`) VALUES
( 'Let Us C', '<p>This is programming language book</p>', 'Balaguruswami', 'Mc Graw Hills', '1998-02-18', 400, 1, 30, 123, '9982374984', '091238797', 'English', 'upload/book/2158fda90d974716504cf1c42a8b3d93'),
( 'Java AWT Reference', '<p>&nbsp;</p>\r\n<p style="margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;">The&nbsp;<strong>Abstract Window Toolkit</strong>&nbsp;(<strong>AWT</strong>) is&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Java (programming language)" href="http://en.wikipedia.org/wiki/Java_(programming_language)">Java</a>''s original platform-dependent&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Windowing system" href="http://en.wikipedia.org/wiki/Windowing_system">windowing</a>,&nbsp;<a class="mw-redirect" style="text-decoration: none; color: #0b0080; background: none;" title="Graphic" href="http://en.wikipedia.org/wiki/Graphic">graphics</a>, and&nbsp;<a class="mw-redirect" style="text-decoration: none; color: #0b0080; background: none;" title="User-interface" href="http://en.wikipedia.org/wiki/User-interface">user-interface</a>&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Widget toolkit" href="http://en.wikipedia.org/wiki/Widget_toolkit">widget toolkit</a>. The AWT is part of the&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Java Foundation Classes" href="http://en.wikipedia.org/wiki/Java_Foundation_Classes">Java Foundation Classes</a>&nbsp;(JFC) &mdash; the standard&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Application programming interface" href="http://en.wikipedia.org/wiki/Application_programming_interface">API</a>&nbsp;for providing a&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Graphical user interface" href="http://en.wikipedia.org/wiki/Graphical_user_interface">graphical user interface</a>&nbsp;(GUI) for a Java program. AWT is also the GUI toolkit for a number of&nbsp;<a style="text-decoration: none; color: #0b0080; background: none;" title="Java Platform, Micro Edition" href="http://en.wikipedia.org/wiki/Java_Platform,_Micro_Edition">Java ME</a>&nbsp;profiles. For example,<a style="text-decoration: none; color: #0b0080; background: none;" title="Connected Device Configuration" href="http://en.wikipedia.org/wiki/Connected_Device_Configuration">Connected Device Configuration</a>&nbsp;profiles require Java runtimes on&nbsp;<a class="mw-redirect" style="text-decoration: none; color: #0b0080; background: none;" title="Mobile telephone" href="http://en.wikipedia.org/wiki/Mobile_telephone">mobile telephones</a>&nbsp;to support AWT.</p>', 'John Zukowski', 'O''Reilly', '1997-02-18', 570, 1, 3, 1045, '1565922409', '9781565922402', 'English', 'upload/book/b9fb9d37bdf15a699bc071ce49baea53'),
( 'Advance Java', '<p style="text-align: justify;"><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">This<strong><em> definitive guide contains complete details on the Java language, its class libraries, and its development environment along with hundreds of examples and expert techniques. Fully updated to cover the late</em></strong>st features of Java 2, v1.4, including the new I/O API, regular expressions, chained exceptions, the assert keyword, and upgrades to Java''s networking classes and the Collections Framework, this comprehensive reference is a must-have for every Java programmer.</span></p>', 'Balaguruswami', 'Nirali Prakashan', '1998-02-18', 300, 2, 11, 998, '7216890796875', '00998277917', 'English', 'upload/book/ddd518a220a0f65d009b15882194a6ff'),
( 'Advance Java', '<p style="text-align: justify;"><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">This<strong><em> definitive guide contains complete details on the Java language, its class libraries, and its development environment along with hundreds of examples and expert techniques. Fully updated to cover the late</em></strong>st features of Java 2, v1.4, including the new I/O API, regular expressions, chained exceptions, the assert keyword, and upgrades to Java''s networking classes and the Collections Framework, this comprehensive reference is a must-have for every Java programmer.</span></p>', 'jasfdb', 'hjnkdc nguy', '1998-02-18', 1276, 1, 11, 998, '7216890796875', '00998277917', 'English', 'upload/book/c8742830b7d13d0f3ce775a75b531a01'),
( 'C++', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">Another gem from Herb Schildt--best-selling programming author with more than 2.5 million books sold! C: The Complete Reference, Fourth Edition gives you full details on C99, the New ANSI/ISO Standard for C. You''ll get in-depth coverage of the C language and function libraries as well as all the newest C features, including restricted pointers, inline functions, variable-length arrays, and complex math. This jam-packed resource includes hundreds of examples and sample applications.</span></p>', 'Herbert Schildt', 'Mc Graw Hills', '2000-04-26', 390, 2, 1, 805, '0072121246', '9780072121247', 'English', 'upload/book/1558ae6e23ea19c12ead996ac062a8fa'),
( 'C: The Complete Reference', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">Herbert Schildt is a leading authority on C and was a member of the ANSI/ISO committee that standardized C. He is the author of Teach Yourself C, C++: The complete Reference, Windows 2000 Programming from the Ground Up, Java: The Complete Reference, and many other best-sellers.</span></p>', 'Herbert Schildt', 'Mc Graw Hills', '2000-04-26', 780, 1, 22, 805, '0928134963', '98798769623798', 'English', 'upload/book/5306a783efd3ea4a94ef406604ebfbb6'),
( 'Microprocessor Design', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>Microprocessor Design</strong> takes entry-level engineers and non-specialists through the fundamentals of microprocessor design, including basic concepts, components, packaging, architectures, circuit design, layout, and testing. More particular-focused and less mathematical than traditional textbooks, this book uses the author''s classroom experience to create an easy-to-follow pedagogy that walks you through the complete design flow process.</span></p>', 'Mcfarland', 'Mc Graw Hills', '2006-06-01', 680, 2, 24, 670, '1234567890', '9876543212345', 'English', 'upload/book/4b84ab3f5759ea89a39185fa1645db50'),
( 'Php: The Complete Reference', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">Covering basic through advanced functionality, this book explains how to develop dynamic Web applications, such as guest books, chat rooms, and shopping carts. All essential topics are included--data handling, object-oriented programming, databases, AJAX, XML, security, and more.</span></p>', 'Steven Holzner', 'Mc Graw Hills', '2007-01-01', 450, 1, 21, 620, '0070223629', '9780070223622', 'English', 'upload/book/2316e1a0d63be7f550459f1685cf04a0'),
( 'Visual Basic.Net: The Complete Reference', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">This book provides in-depth information on the new object-oriented capabilities of Visual Basic .NET and details on the core language, including grammar, control-flow, operators, value-types, classes, interfaces, data structures and collections, delegates, GUI components, threading, and debugging.</span></p>', 'Shapiro', 'Mc Graw Hills', '2002-01-01', 500, 2, 8, 870, '0070495114', '9780070495111', 'English', 'upload/book/bab6e141a2242a62e5ada2a7ee8a90b1'),
( 'Mysql: The Complete Reference', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">Get comprehensive coverage of all the powerful new features of MySQL, one of the fastest--and free--relational databases in use today. Written in conjunction with the MySQL development team, this expert resource covers transactional integrity, disaster recovery, scalability, support for mobile users, Web-based and client/server programming, and much more.</span></p>', 'Vaswani', 'Mc Graw Hills', '2004-01-04', 389, 1, 14, 870, '0070586845', '9780070586840', 'English', 'upload/book/b8e408ff8daec509b3bd510ffe31f8d6'),
( 'The Complete Android Guide 2nd Edition', '<p><span style="color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">Developers, build mobile Android apps using Android 4</span></p>\r\n<p style="margin: 0px 0px 1.3em; padding: 0px; color: #333333; font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;">The fast-growing popularity of Android smartphones and tablets creates a huge opportunities for developers. If you''re an experienced developer, you can start creating robust mobile Android apps right away with this professional guide to Android 4 application development. Written by one of Google''s lead Android developer advocates, this practical book walks you through a series of hands-on projects that illustrate the features of the Android SDK.</p>', 'Kevin Purdy', '3ones Inc', '1990-09-09', 650, 1, 15, 900, '0982592647', '9780982592649', 'English', 'upload/book/1251d5c43aa8d35f93ebf960315c2f6d');


/*-----------------users_addresses----------------*/

drop table if exists users_addresses;

-- ---------------------------------------
	
CREATE TABLE `users_addresses` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `address1` varchar(64) NOT NULL,
 `address2` varchar(64) NOT NULL,
 `city` varchar(64) NOT NULL,
 `state` varchar(64) NOT NULL,
 `pincode` int(11) NOT NULL,
 `landmark` varchar(64) DEFAULT NULL,
 `country` varchar(64) NOT NULL,
 `contact` varchar(16) NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) NOT NULL DEFAULT '1',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





/*-------------------configs-----------------*/

drop table if exists configs;

-- ----------------------------------
	
CREATE TABLE `configs` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `val` text NOT NULL,
 `oftype` varchar(128) NOT NULL,
 `option` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*---------------------feedback-----------------------*/

drop table if exists feedback;

-- -------------------------------------------
CREATE TABLE `feedback` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `message` text NOT NULL,
 `feedback_purpose` text NOT NULL,
 `user_id` int(11) NOT NULL,
 `creationn_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*--------------------genres----------------------*/

drop table if exists genres;

-- ------------------------------------
	
CREATE TABLE `genres` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `descrioption` text NOT NULL,
 `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `lastmodified_ts` timestamp NULL DEFAULT NULL,
 `is_active` int(1) NOT NULL DEFAULT '1',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`name`, `descrioption`) VALUES
('Art', ''),
('Anthologies', ''),
('Autobiographies', ''),
('Biographies', ''),
('Children''s', ''),
('Comics', ''),
('Cookbooks', ''),
('Diaries', ''),
('Dictionaries', ''),
('Drama', ''),
('Encyclopedias', ''),
('Fantasy', ''),
('fiction', ''),
('Guide', ''),
('help', ''),
('Horror', ''),
('Journals', ''),
('Math', ''),
('Mystery', ''),
('Poetry', ''),
('Prayer', ''),
('Religious', ''),
('Romance', ''),
('Satire', ''),
('Science', ''),
('Self', ''),
('Series', ''),
('Travel', ''),
('Trilogies', ''),
('Other', '');

/*---------------------swap---------------------*/

drop table if exists swap;

-- ------------------------------------------

CREATE TABLE `swap` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'It is unique id for each swap',
  `user_id` int(11) NOT NULL COMMENT 'It is use to store user_id',
  `book_id` int(11) NOT NULL COMMENT 'It is use to store book_id',
  `swapped_book_id` int(11) NOT NULL COMMENT 'It is use to store swapped_book_id',
  `swapped_user_id` int(11) NOT NULL COMMENT 'It is use to store swapped_user_id',
  `user_phone` varchar(11) NOT NULL DEFAULT '0',
  `swapped_user_phone` varchar(11) NOT NULL DEFAULT '0',
  `is_approved` int(1) NOT NULL DEFAULT '0' COMMENT 'It is approved by swapped user',
  `access_token` varchar(128) NOT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'It is use to store creation time of the swap',
  `lastmodified_ts` timestamp NULL DEFAULT NULL COMMENT 'It is use to store modified time of the swap',
  PRIMARY KEY (`id`),
  UNIQUE KEY `book_id` (`book_id`),
  UNIQUE KEY `swapped_book_id` (`swapped_book_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

/*---------------------hits---------------------*/

drop table if exists hits;

-- -----------------------------------------



CREATE TABLE `hits` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(128) NOT NULL,
  `os` varchar(128) NOT NULL,
  `browser` varchar(128) NOT NULL,
  `page` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



