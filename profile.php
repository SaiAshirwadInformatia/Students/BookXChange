<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['logged'] ) or $_SESSION ['logged'] != true) {
	header ( "Location: index.php" );
	$_SESSION ['please_login'] = true;
	exit ();
} else {
	$page = "profile.php";
	require_once 'hits.php';
	require_once 'db_connect.php';
	if (isset ( $_GET ['user_id'] )) {
		$book_user_id = $_GET ['user_id'];
	} elseif (isset ( $_SESSION ['loggeduser'] )) {
		$book_user_id = $_SESSION ['loggeduser'] ['id'];
	}
	$query = "SELECT * FROM users WHERE id = $book_user_id";
	$book_user = $mysqli->query ( $query );
	if (is_object ( $book_user ) and $book_user->num_rows > 0) {
		$book_user = $book_user->fetch_assoc ();
	} else {
		header ( "Location: index.php" );
		exit ();
	}
	
	$start = 0;
	$limit = 4;
	if ($_GET ['page']) {
		$page = $_GET ['page'];
		$start = ($page - 1) * $limit;
	} else {
		$page = 1;
	}
	$query = "SELECT * FROM books WHERE user_id = $book_user_id AND id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND is_approved = 1 ORDER BY id desc LIMIT $start, $limit";
	$books = $mysqli->query ( $query );
	
	$query = "SELECT * FROM users_addresses WHERE user_id = " . $book_user_id;
	$users_addresses = $mysqli->query ( $query );
	$user_addresses = $users_addresses->fetch_assoc ();
	
	require_once 'inc_header.php';
	require_once 'inc_nav.php';
	?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li class="active"><span class="glyphicon glyphicon-user"></span>
			Profile <span class="glyphicon glyphicon-arrow-right"></span> <?php echo $book_user['fname'];?></li>
		<?php if(!isset($_GET['user_id'])){?>
		<li><a href="profile_update.php"><span
				class="glyphicon glyphicon-pencil"></span> Update Profile</a></li>
				<?php }?>
	</ol>
	<div class="row">
		<div class="col-md-12 " style="margin-top: 10px">
			<div class="col-md-8">
				<?php
	
	echo '<h2>' . ucfirst ( $book_user ['fname'] ) . ' ' . ucfirst ( $book_user ['lname'] );
	if ($_SESSION ['loggeduser'] ['id'] == $book_user ['id']) {
		echo ' <small class="pull-right">
        <a href="profile_update.php" 
            class="btn btn-primary btn-xs" title="Update your Profile">
                <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <a href="change_password.php" title="Update your Password"
               class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-cog"></span>
        </a>
    </small>';
		echo '</h2>';
		?>
			<div class="col-md-6">
					<h5><?php echo '<strong>DOB : '.(($book_user['dob'] == '0000-00-00')?'':$book_user['dob']).'<br>';?>
				<?php echo 'Mobile : '.$book_user['phone'].'<br>';?>
				<?php echo 'Email Id : '.$book_user['email'].'<br>';?>
				<?php echo 'Blood Group : '.$book_user['blood_group'].'<br></strong>';?></h5>
				</div>
				<div class="col-md-6">
					<h5><?php echo '<strong>Address <br>	'.$user_addresses['address1'].'<br>';?>
				 <?php echo $user_addresses['address2'].'<br></strong>';?></h5>
				</div>
				<?php
	} else {
		echo '</h2>';
		?>
					<div class="col-md-6">
					<h5><?php echo '<strong>Live at <br>'.$user_addresses['address2'].'<br>'.$user_addresses['state'].'  '.$user_addresses['country'].'</strong>';?></h5>
				</div>
				<?php
	}
	?>
			</div>
			<div class="col-md-4">
				<img
					src="<?php echo (isset($book_user['display_picture'])?$book_user['display_picture']:'images/default_user.jpg');?>"
					style="height: 200px; width: 150px" class="img img-thumbnail" />
			</div>
		</div>
	</div>
	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-2">
	
		<?php
	if (isset ( $_SESSION ['logged'] ) and $_SESSION ['loggeduser'] ['id'] == $book_user ['id']) {
		echo '<a class="btn btn-success btn-block pull-right" href="edit_book.php" title="Add Book"><span class="glyphicon glyphicon-book"></span> Add Book</a>';
	}
	?>
    </div>

	</div>
	<div class="row">
		  
			<?php
	if ($books !== false and $books->num_rows > 0) {
		?>
      	<?php
		
		if ($_SESSION ['loggeduser'] ['id'] == $book_user ['id']) {
			echo '<h4>My Books</h4>';
		} else {
			?>
		<h4>Books Added By <?php echo ucfirst($book_user['fname']);?></h4>
		<?php }?>
		<hr>

			<?php
		while ( ($book = $books->fetch_assoc ()) != null ) {
			require 'inc_book.php';
		}
		echo '</div>';
		?>
		<?php
	} else {
		echo '<div class="col-md-12"><div class="alert alert-danger" role="alert"><p>No books added by ';
		if ($_SESSION ['loggeduser'] ['id'] == $book_user ['id']) {
			echo "you";
		} else {
			echo $book_user ['fname'];
		}
		echo '</p></div></div>';
	}
	?>
	<?php
	$query = "SELECT * FROM books WHERE is_approved = 1 and user_id= $book_user_id and id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap)";
	$rows = $mysqli->query ( $query )->num_rows;
	$total = ceil ( $rows / $limit );
	?>
		<div align="center">
			<ul class="pagination">
		<?php for($i = 1 ; $i <= $total; $i++){?>
			<li><a
					href="profile.php?user_id=<?php echo $book_user_id;?>&page=<?php echo $i;?>"
					class="btn btn-primary <?php if($i == $page)echo "active";?>"><?php echo $i;?></a></li>
		<?php }?>
		</ul>
		</div>

	</div>
</div>

<?php
}
?>
<?php

require_once 'inc_footer.php';

?>
