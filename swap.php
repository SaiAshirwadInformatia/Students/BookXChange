<?php
require_once 'db_connect.php';
if (! isset ( $_SESSION ['logged'] ) and empty ( $_SESSION ['logged'] ) == true) {
	$_SESSION ['please_login'] = true;
	header ( "Location: index.php" );
	exit ();
}

// Getting value from URL
$swapped_book_id = $_GET ['swapped_book_id'];

if (isset ( $swapped_book_id )) {
	
	// Query for Swapped book Details
	$query = "SELECT * FROM books WHERE id = $swapped_book_id";
	$swapped_books = $mysqli->query ( $query );
	$swapped_book = $swapped_books->fetch_assoc ();
	
	// Query for genre of swapped book
	$query = "SELECT * FROM genres WHERE id =" . $swapped_book ['genre_id'];
	$genre = $mysqli->query ( $query );
	$swapped_book_genre = $genre->fetch_assoc ();
	
	// Query for user books list
	$query = "SELECT * FROM books WHERE user_id = '" . $_SESSION ['loggeduser'] ['id'] . "' AND is_approved = 1 AND id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap)";
	$books = $mysqli->query ( $query );

	
	// Query for swap table
	$query = "SELECT * FROM swap";
	$swap = $mysqli->query ( $query );
}

if (isset ( $_GET ['swapped_book_id'], $_GET ['book_id'], $_GET ['user_id'] )) {
	
	// Getting value from URL
	$book_id = $_GET ['book_id'];
	$swapped_user_id = $swapped_book ['user_id'];
	
	// Query for user book details
	$query = "SELECT * FROM books WHERE id = $book_id";
	$user_books = $mysqli->query ( $query );
	$user_book = $user_books->fetch_assoc ();
	
	// Query for genre of user book
	$query = "SELECT * FROM genres WHERE id = " . $user_book ['genre_id'];
	$genre = $mysqli->query ( $query );
	$user_book_genre = $genre->fetch_assoc ();
}

require_once 'hits.php';

require_once 'inc_header.php';
require_once 'inc_nav.php';

?>

<div class="col-md-11 col-sm-11">
	<div class="col-md-6 col-sm-12">
		<div class="col-md-12">
			<h4>Swap Book: <?php echo $swapped_book['name'];?></h4>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="thumbnail">
				<img src="<?php echo $swapped_book['cover_picture']?>"
					alt="<?php echo $swapped_book['name'];?>"
					style="height: 334px; width: 226px">
			</div>
		</div>
		<table>
			<tr>
				<td>Genre</td>
				<td>:</td>
				<td><?php echo  $swapped_book_genre['name']  ; ?></td>
			</tr>
			<tr>
				<td>Author</td>
				<td>:</td>
				<td><?php echo  $swapped_book['author']  ; ?></td>
			</tr>
			<tr>
				<td>Pages</td>
				<td>:</td>
				<td><?php echo  $swapped_book['pages']  ;?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td>:</td>
				<td><?php echo $swapped_book['price'];?></td>
			</tr>
			<tr>
				<td>ISBN 13</td>
				<td>:</td>
				<td><?php echo  $swapped_book['isbn13']  ; ?></td>
			</tr>
			<tr>
				<td>ISBN 10</td>
				<td>:</td>
				<td><?php echo  $swapped_book['isbn10']  ; ?></td>
			</tr>
		</table>
	</div>
	<?php
	if (isset ( $_GET ['user_id'] )) {
		?>
	<div class="col-md-6 col-sm-12">
		<div class="col-md-12">
			<h4>With : <?php echo $user_book['name'];?></h4>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="thumbnail">
				<img src="<?php echo $user_book['cover_picture']?>"
					alt="<?php echo $user_book['name'];?>"
					style="height: 334px; width: 226px">
			</div>
		</div>
		<table>
			<tr>
				<td>Genre</td>
				<td>:</td>
				<td><?php echo  $user_book_genre['name']  ; ?></td>
			</tr>
			<tr>
				<td>Author</td>
				<td>:</td>
				<td><?php echo  $user_book['author']  ; ?></td>
			</tr>
			<tr>
				<td>Pages</td>
				<td>:</td>
				<td><?php echo  $user_book['pages']  ;?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td>:</td>
				<td><?php  echo $user_book['price'];?></td>
			</tr>
			<tr>
				<td>ISBN 13</td>
				<td>:</td>
				<td><?php echo  $user_book['isbn13']  ; ?></td>
			</tr>
			<tr>
				<td>ISBN 10</td>
				<td>:</td>
				<td><?php echo  $user_book['isbn10']  ; ?></td>
			</tr>
		</table>
	</div>
	<?php } ?>
	
	<?php
	$query = "SELECT * FROM books WHERE user_id = " . $swapped_book ['user_id'] . " AND id NOT IN(SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap)";
	$show = $mysqli->query ( $query );
	if (is_object ( $show ) and $show->num_rows > 0) {
		?>
	<div class="col-md-12">
		<form action="" method="GET">
			<div class="col-md-4 col-sm-8">
				<input type="hidden" name="swapped_book_id"
					value="<?php echo $swapped_book_id?>" /> <input type="hidden"
					name="swapped_user_id"
					value="<?php  echo $swapped_book['user_id'];?>" /> <input
					type="hidden" name="user_id" value="<?php echo $user_id;?>" />
					<?php   if(isset($books) and $books->num_rows > 0){?> 
					<select class="form-control" id="select" name="book_id" style="margin-top:15px">
	  <?php
			
			while ( ($book = $books->fetch_assoc ()) != null ) {
				$query = "SELECT * FROM swap WHERE book_id = " . $book ['id'] . " or swapped_book_id = " . $book ['id'] . " ";
				$swaps = $mysqli->query ( $query );
				if (isset ( $swaps ) and ! empty ( $swaps ) and $swaps->num_rows > 0) {
				} else {
					echo '<option value="' . $book ['id'] . '"' . (($book ['id'] == $_GET ['book_id']) ? 'selected' : '') . '>' . $book ['name'] . '</option>';
				}
			}
			
			?>
	                  </select>
			</div>
			<div class="col-md-2 col-sm-4">
				<button type="submit" class="btn btn-primary btn-block">Compare</button>
			</div>
		</form>
		<div class="col-md-2 col-sm-4">
		<?php if (isset($_GET['swapped_book_id'], $_GET['book_id'], $_GET['user_id'])) {?>
			<a class="btn btn-success btn-block"
				href="contact_sharing.php?user_id=<?php echo $user['id'];?>&book_id=<?php echo $user_book['id'];?>&swapped_user_id=<?php
				echo $swapped_book ['user_id'];
				?>&swapped_book_id=<?php echo $swapped_book_id;?>"> Swap</a>
		</div>
 
 <?php
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><p>Before comparing the book you should added one book in your account.</p></div>';
		}
		?>
	</div>
	<?php
	} else {
		echo '<div class="col-md-12 alert alert-danger"><p>Before swapping the book you should added one book in your account.</p></div>';
	}
	?>
</div>
</div>

<?php
require_once 'inc_footer.php';
?>