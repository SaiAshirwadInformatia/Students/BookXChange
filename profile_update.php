<?php

require_once 'db_connect.php';

if (! isset ( $_SESSION ['logged'] ) or $_SESSION ['logged'] == false) {
	header ( "Location: index.php" );
}

$page = "profile_update.php";
require_once 'hits.php';

$user = $_SESSION ['loggeduser'];

$fields = array (
		"fname",
		"lname",
		"phone",
		"gender",
		"blood_group",
		"dob",
		"display_picture" 
);
$fields2 = array (
		"address1",
		"address2",
		"country",
		"state",
		"city",
		"pincode",
		"landmark" 
);

$query = "SELECT * FROM users WHERE id = " . $user ['id'];
$user = $mysqli->query ( $query );
$user = $user->fetch_assoc ();

$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
$user_addresses = $mysqli->query ( $query );
$user_address = $user_addresses->fetch_assoc ();

$user_id = $user ['id'];
if (isset ( $_POST ) and count ( $_POST ) > 0) {
	$query = "SELECT * FROM users_addresses";
	$ids = $mysqli->query ( $query );
	if ($ids->num_rows > 0) {
		$query = "SELECT * FROM users_addresses WHERE user_id = $user_id";
		$rows = $mysqli->query ( $query );
		if (isset ( $rows ) and $rows->num_rows > 0) {
			
			$set_array2 = array ();
			$query = "UPDATE users_addresses SET ";
			foreach ( $fields2 as $field2 ) {
				if (isset ( $_POST ) and ! empty ( $_POST )) {
					$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
				}
			}
			$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
			if ($mysqli->query ( $query ) === TRUE) {
				$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
				$user_addresses = $mysqli->query ( $query );
				$user_address = $user_addresses->fetch_assoc ();
			}
		} else {
			$query = "INSERT INTO users_addresses (user_id) VALUES ($user_id)";
			if ($mysqli->query ( $query )) {
				$set_array2 = array ();
				$query = "UPDATE users_addresses SET ";
				foreach ( $fields2 as $field2 ) {
					if (isset ( $_POST ) and ! empty ( $_POST )) {
						$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
					}
				}
				$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
				if ($mysqli->query ( $query ) === TRUE) {
					$_SESSION['profile_updated'] = true;
					$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
					$user_addresses = $mysqli->query ( $query );
					$user_address = $user_addresses->fetch_assoc ();
				}
			}
		}
	} else {
		$query = "INSERT INTO users_addresses (user_id) VALUES ($user_id)";
		if ($mysqli->query ( $query )) {
			$set_array2 = array ();
			$query = "UPDATE users_addresses SET ";
			foreach ( $fields2 as $field2 ) {
				if (isset ( $_POST ) and ! empty ( $_POST )) {
					$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
				}
			}
			$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
			if ($mysqli->query ( $query ) === TRUE) {
				$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
				$user_addresses = $mysqli->query ( $query );
				$user_address = $user_addresses->fetch_assoc ();
			}
		} else {
			echo "User ID didn't passed in user_addresses";
		}
	}
	$isValid = true;
	$set_array = array ();
	$query = "UPDATE users
            SET ";
	foreach ( $fields as $field ) {
		if (isset ( $_POST [$field] ) or isset ( $_FILES ['display_picture'] ) and ! empty ( $_POST [$field] ) or ! empty ( $_FILES ['display_picture'] )) {
			if ($field == "display_picture") {
				if ($_FILES ['display_picture'] ['error'] == 0) {
					$query1 = "SELECT * FROM users WHERE id = " . $user ['id'];
					$pic = $mysqli->query ( $query1 );
					$pic = $pic->fetch_assoc ();
					if (isset ( $pic ['display_picture'] )) {
						if (! unlink ( $pic ['display_picture'] )) {
							echo "Deletion Failed";
						}
					}
					$move = md5 ( $_FILES ['display_picture'] ['name'] . time () );
					move_uploaded_file ( $_FILES ['display_picture'] ['tmp_name'], "upload/" . $move );
					$set_array [] = "$field = 'upload/" . $move . "'";
				}
			} else {
				if ($field == "phone" and isset ( $_POST ['phone'] ) and ! empty ( $_POST ['phone'] ) == true) {
					if (is_numeric ( $_POST [$field] ) and (strlen ( $_POST [$field] ) == 10)) {
						$set_array [] = "$field = '" . $_POST [$field] . "'";
					} else {
						$isValid = false;
					}
				} else {
					$set_array [] = "$field = '" . $mysqli->escape_string ( $_POST [$field] ) . "'";
				}
			}
		}
	}
	$last_modified = date ( "Y-m-d h:m:s", time () );
	$query .= implode ( ",", $set_array ) . ", lastmodified_ts = '$last_modified' WHERE id = " . $user ['id'];
	if ($isValid) {
		if ($mysqli->query ( $query ) === TRUE) {
			$_SESSION['profile_updated'] = true;
			$query = "SELECT * FROM users WHERE id = " . $user ['id'];
			$users = $mysqli->query ( $query );
			$user = $users->fetch_assoc ();
			$_SESSION ['loggeduser'];
			echo '<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Your Profile Updated Successfully</strong>
</div>';
		} else {
			echo "Error updating record: " . $mysqli->error;
		}
	} else {
		echo '<div class="alert alert-warning alert-dismissible" role="alert">
            <button  class="close" data-dismiss="alert" type="button">
            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <strong>Please enter correct phone number.</strong>
            </div>';
	}
}

if(isset($_SESSION['profile_updated']) and !empty($_SESSION['profile_updated']) == true){
	$mail->addAddress($user['email'], $user['fname'] .' '. $user['lname']);
	$message = file_get_contents('templates/update_profile.txt');
	$message = str_replace('*|FNAME|*', $user['fname'], $message);
	$message = nl2br($message);
	$mail->msgHTML($message);
	$mail -> Subject = "Profile Updated - BookXchange";
	if(!$mail->send()){
		echo $mail->ErrorInfo;
	}
	unset($_SESSION['profile_updated']);
}

require_once 'inc_header.php';
require_once 'inc_nav.php';
?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="profile.php"><span class="glyphicon glyphicon-user"></span>
				Profile <span class="glyphicon glyphicon-arrow-right"></span> <?php echo $user['fname'];?></a></li>
		<li class="active"><span class="glyphicon glyphicon-pencil"></span>
			Update Profile</li>
	</ol>
	<form class="form-horizontal" action="" method="POST"
		enctype='multipart/form-data'>
		<fieldset>
			<legend>Personal Information</legend>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="fname">First Name </label> <input type="text"
					name="fname" class="form-control"
					value="<?php echo $user["fname"] ?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="lname">Last Name</label> <input type="text"
					class="form-control" name="lname"
					value="<?php echo $user["lname"]?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="username">Username</label> <input type="text"
					class="form-control" name="username"
					value="<?php echo $user["username"]?>" disabled />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="email">Email ID</label> <input type="email"
					class="form-control" name="email"
					value="<?php echo $user["email"]?>" disabled />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="phone">Phone No.</label> <input type="number"
					class="form-control" name="phone"
					value="<?php echo $user["phone"]?>" data-hint ="Enter valid phone number" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="dob">Date of Birth</label> <input type="date"
					placeholder="yyyy-mm-dd" class="form-control" name="dob"
					value="<?php if($user["dob"] == '0000-00-00'){echo '';}else {echo $user["dob"];}?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="gender">Gender</label>
				<div class="radio radio-primary">
					<?php if($user["gender"] == "m"){?>
						<label> <input type="radio" name="gender" value="m" checked />
						Male
					</label> <label> <input type="radio" name="gender" value="f" />
						Female
					</label>
						<?php }else{?>
						<label> <input type="radio" name="gender" value="m" /> Male
					</label> <label> <input type="radio" name="gender" value="f"
						checked /> Female
					</label><?php }?>
					</div>
			</div>
			<?php
			$blood_group = array (
					"A+",
					"A-",
					"B+",
					"B-",
					"O+",
					"O-",
					"AB+",
					"AB-" 
			);
			?>
			
			
			<div class="col-md-4" style="padding-top: 15px">
				<label>Blood Group</label> <select name="blood_group" id="select"
					class="form-control">
					<option value="">Select</option>
					<?php
					foreach ( $blood_group as $blood_group ) {
						echo '<option value="' . $blood_group . '"' . (($user ['blood_group'] == $blood_group) ? ' selected' : '') . '>' . $blood_group . '</option>';
					}
					?>
				</select>
			</div>
			<div class="col-md-1" style="padding-top: 15px">
				<img
					src="<?php echo (isset($user['display_picture']) ? $user['display_picture'] : '') ;?>"
					height="100px" width="100px">
			</div>
			<div class="col-md-3" style="padding-top: 15px">
				<label>Profile Picture</label> <input type="file"
					name="display_picture" class="form-control" />
			</div>
		</fieldset>
		<fieldset>
			<legend>Address</legend>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Address1</label> <input type="text" name="address1"
					class="form-control"
					value="<?php echo isset($user_address['address1'])?$user_address['address1']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Address2</label> <input type="text" class="form-control"
					name="address2"
					value="<?php echo isset($user_address['address2'])?$user_address['address2']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>City</label> <input type="text" class="form-control"
					name="city"
					value="<?php echo isset($user_address['city'])?$user_address['city']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>State</label> <input type="text" class="form-control"
					name="state"
					value="<?php echo isset($user_address['state'])?$user_address['state']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Country</label> <input type="text" class="form-control"
					name="country"
					value="<?php echo isset($user_address['country'])?$user_address['country']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Pincode</label> <input type="number" class="form-control"
					name="pincode"
					value="<?php  if($user_address['pincode'] == 0){echo '';}else{echo $user_address['pincode'];}?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Landmark</label> <input type="text" class="form-control"
					name="landmark"
					value="<?php echo isset($user_address['landmark'])?$user_address['landmark']:' ';?>" />
			</div>
		</fieldset>
		<div class="row" style="padding: 15px" align="center">
			<button class="btn btn-primary" type="submit">Save Changes</button>
		</div>
	</form>
</div>

