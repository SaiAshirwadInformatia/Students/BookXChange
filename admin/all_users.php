<?php


require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}


$start = 0;
$limit = 3;
if (isset($_GET ['page'])) {
	$page = $_GET ['page'];
	$start = ($page - 1) * $limit;
}
if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
	$query = "SELECT * FROM users WHERE fname like '%" . $_POST ['search'] . "%' or lname like '%" . $_POST ['search'] . "%' LIMIT $start,$limit";
	if ($mysqli->query ( $query )) {
		$alluser = $mysqli->query ( $query );
	} else {
		echo "Please not user Apostrophy";
		$query = "SELECT * FROM users ORDER BY fname LIMIT $start,$limit";
		$alluser = $mysqli->query ( $query );
	}
} else {
	$query = "SELECT * FROM users ORDER BY fname LIMIT $start,$limit";
	$alluser = $mysqli->query ( $query );
}
$i = 1;

if (isset ( $_SESSION ['success_msg'] ) and ! empty ( $_SESSION ['success_msg'] ) == true) {
	echo '<div class="alert alert-success">' . $_SESSION ['success_msg'] . '</div>';
	unset ( $_SESSION ['success_msg'] );
}

if (isset ( $_SESSION ['error_msg'] ) and ! empty ( $_SESSION ['error_msg'] ) == true) {
	echo '<div class="alert alert-danger">' . $_SESSION ['error_msg'] . '</div>';
	unset ( $_SESSION ['error_msg'] );
}


require_once 'inc_header.php';

require_once 'inc_nav.php';
?>
<script>
function confirmAction(){
    var con =  confirm("Are you sure to delete the User");
    return con;    
}
</script>
<div class="col-md-9">
	<div class="col-md-6"></div>
	<form action="" method="POST">
		<div class="col-md-4">
			<input type="text" class="form-control" name="search"
				placeholder="search ny fname/lname"
				value="<?php
				if (isset ( $_POST ['search'] ) and ! empty ( $_POST ['search'] ) == true) {
					echo $_POST ['search'];
				}
				?>" />
		</div>
		<button type="submit" class="btn btn-primary" style="margin: -5px">Search</button>
	</form>
	<table class="table table-striped table-hover ">
		<thead>
			<tr>
				<th><span class=" glyphicon glyphicon-user"></span></th>
				<th>Name</th>
				<th>Email ID</th>
				<th>Username</th>
				<th>Contact</th>
				<th>Gender</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php while (($user = $alluser->fetch_assoc()) != null){?>
			<tr>
				<td><a href="user_info.php?user_id=<?php echo $user['id'];?>"><img
						src="<?php if(isset($user['display_picture'])){echo "../".$user['display_picture'];}?>"
						height="50px" width="35px"></a></td>
				<td><a href="user_info.php?user_id=<?php echo $user['id'];?>"><?php echo $user['fname'].' '.$user['lname'];?></a></td>
				<td><?php echo $user['email'];?></td>
				<td><?php echo $user['username'];?></td>
				<td><?php echo $user['phone'];?></td>
				<td><?php echo ($user['gender'] == "m")?"Male":"Female";?></td>
				<td><div class="btn-group">
						<a href="update_user.php?user_id=<?php echo $user['id'];?>"
							class="btn btn-primary btn-xs"><span
							class="glyphicon glyphicon-pencil"></span></a> <a
							href="delete_user.php?user_id=<?php echo $user['id']; ?>"
							class="btn btn-danger btn-xs"><span
							class="glyphicon glyphicon-trash"></span></a>
					</div></td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>

	<?php
	$query = "SELECt * FROM users";
	$rows = $mysqli->query ( $query )->num_rows;
	$total = ceil ( $rows / $limit );
	?>
	<div align="center">
		<ul class="pagination">
		<?php for($i = 1; $i <= $total; $i++){?>
			<li><a href="all_users.php?page=<?php echo $i;?>"
				class="btn btn-primary <?php if($i == $page)echo "active";?>"><?php echo $i;?></a></li>
		<?php }?>
		</ul>
	</div>
</div>