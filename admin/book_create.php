<?php

require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}


$query = "SELECT * FROM genres";
$genres = $mysqli->query ( $query );

$query = "SELECT * FROM users";
$users = $mysqli->query ( $query );
$isValid = true;
if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
	$fields = array (
			"name",
			"author",
			"published",
			"price",
			"published_year",
			"language",
			"genre_id",
			"pages",
			"isbn13",
			"isbn10",
			"description",
			"user_id",
			"cover_picture" 
	);
	foreach ( $fields as $field ) {
		if ($field == "cover_picture") {
			if (isset ( $_FILES ['cover_picture'] ) and !empty($_FILES['cover_picture']) == true) {
				if ($_FILES ['cover_picture'] ['error'] == 0) {
					$move = "upload/book/".md5 ( $_FILES ['cover_picture'] ['tmp_name'] );
					$_POST ['cover_picture'] = $move;
					$move = "../" . $move;
					move_uploaded_file ( $_FILES ['cover_picture'] ['tmp_name'], $move );
					$$field = $_POST ['cover_picture'];
					$isValid = true;
				}else{
					$isValid = false;
					break;
				}
			}		
		} else if (isset ( $_POST [$field] ) and ! empty ( $_POST [$field] ) == true and $isValid) {
			if (is_string ( $_POST [$field] ) ) {
				$$field = $mysqli->escape_string ( $_POST [$field] );
				$isValid = true;
			}
		} else {
			$isValid = false;
			break;
		}
	}
	if ($isValid) {
		$query = "INSERT INTO books (" . implode ( ",", $fields ) . ")";
		$query .= "VALUES ('$name', '$author', '$published', '$price', '$published_year', '$language', '$genre_id', '$pages'";
		$query .= ", '$isbn13', '$isbn10', '$description', '$user_id', '$cover_picture')";
		if ($mysqli->query ( $query )) {
			?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Book Created Successfully.</strong>
</div>
<?php
		} else {
			?>
<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Book Not created.</strong>
</div>
<?php
		}
	} else {
		?>
<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Please fill all the required fields</strong>
</div>
<?php
	}
}

require_once 'inc_header.php';

require_once 'inc_nav.php';

?>

<div class="col-md-9">
	<form class="form-horizontal" action="" method="POST"
		enctype="multipart/form-data">
		<fieldset>
			<h4>Enter Information About Book</h4>
			<hr>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Name</label>
				<div class="col-lg-9">
					<input type="hidden" name="user_id" /> <input type="text"
						class=" form-control" name="name" id="inputDefault"
						placeholder="(ex: Lord of Rings)">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Author</label>
				<div class="col-lg-9">
					<input type="text" class=" form-control" name="author"
						id="inputDefault" placeholder="(ex: J. R. R. Tolkien)">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publisher</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="published"
						placeholder="(ex: George Allen & Unwin ">
				</div>
				<label for="textArea" class="col-md-2 control-label">Price</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="price"
						placeholder="(ex: 230)">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publication
					Date</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="published_year"
						placeholder="(ex: YYYY-MM-DD">
				</div>
				<label for="textArea" class="col-md-2 control-label">Language</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="language"
						placeholder="(ex: English)">
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">Genre</label>
				<div class="col-md-3">
					<select class="form-control" id="select" name="genre_id">
						<option>--Select--</option>
				    	   	<?php
													while ( ($genre = $genres->fetch_assoc ()) != null ) {
														echo '<option value="' . $genre ['id'] . '"' . (($genre ['id'] == $book ['genre_id']) ? 'selected' : '') . '>' . $genre ['name'] . '</option>';
													}
													?>
					</select>
				</div>
				<label for="inputDefault" class="col-md-2 control-label">Pages</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="pages"
						id="inputEmail" placeholder="(ex: 350)">
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">ISBN-13 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn13"
						id="inputPassword" placeholder="(ex: 9780689869914)">
				</div>
				<label for="inputDefault" class="col-md-2 control-label">ISBN-10 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn10"
						id="inputPassword" placeholder="(ex: 9780689869)">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Description</label>
				<div class="col-md-9">
					<textarea class="form-control" id="textArea"
						placeholder="Any information about Book condition / features"
						name="description"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Book Image</label>
				<div class="col-md-9">
					<input type="file" name="cover_picture" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Select User</label>
				<div class="col-md-4">
					<select class="form-control" id="select" name="user_id">
						<option>Select</option>
						<?php
						while ( ($user = $users->fetch_assoc ()) != null ) {
							echo '<option value="' . $user ['id'] . '">' . $user ['fname'] . ' ' . $user ['lname'] . '</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-9 col-lg-offset-2">
					<button type="submit" class="btn btn-primary">Add Book</button>
				</div>
			</div>
		</fieldset>
	</form>
	<script>
$(document).ready(function(){
    tinymce
	.init({
	    selector : "textarea",
	    relative_urls : true,
	    remove_script_host : false
	});
});
</script>
</div>