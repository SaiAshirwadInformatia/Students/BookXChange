<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}

$start = 0;
$limit = 3;
if ($_GET ['page']) {
	$page = $_GET ['page'];
	$start = ($page - 1) * $limit;
}
if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
	$query = "SELECT * FROM books WHERE (name like '%" . $_POST ['search'] . "%' or author like '%" . $_POST ['search'] . "%'";
	$query .= " or published like '%" . $_POST ['search'] . "%') AND `is_approved` = 1 LIMIT $start, $limit";
	if ($mysqli->query ( $query )) {
		$books = $mysqli->query ( $query );
	} else {
		echo "Please not user Apostrophy";
		$query = "SELECT * FROM books WHERE `is_approved` = 1 ORDER BY name LIMIT $start, $limit";
		$books = $mysqli->query ( $query );
	}
} else {
	$query = "SELECT * FROM books WHERE `is_approved` = 1 ORDER BY name LIMIT $start, $limit";
	$books = $mysqli->query ( $query );
}

if (isset ( $_SESSION ['success_msg'] ) and ! empty ( $_SESSION ['success_msg'] ) == true) {
	echo '<div class="alert alert-success">' . $_SESSION ['success_msg'] . '</div>';
	unset ( $_SESSION ['success_msg'] );
}

if (isset ( $_SESSION ['error_msg'] ) and ! empty ( $_SESSION ['error_msg'] ) == true) {
	echo '<div class="alert alert-danger">' . $_SESSION ['error_msg'] . '</div>';
	unset ( $_SESSION ['error_msg'] );
}

require_once 'inc_header.php';

require_once 'inc_nav.php';
?>


<div class="col-md-9">
		<div class="col-md-6"></div>
	<form action="" method="POST">
		<div class="col-md-4">
			<input type="text" class="form-control" name="search"
				placeholder="search ny fname/lname"
				value="<?php
				if (isset ( $_POST ['search'] ) and ! empty ( $_POST ['search'] ) == true) {
					echo $_POST ['search'];
				}
				?>" />
		</div>
		<button type="submit" class="btn btn-primary" style="margin: -5px">Search</button>
	</form>
	<table class="table table-striped table-hover ">
		<thead>
			<tr>
				<th>Book Picture</th>
				<th>Name</th>
				<th>Author</th>
				<th>Publisher</th>
				<th>Genre</th>
				<th>Added By</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php while (($book = $books->fetch_assoc()) != null){?>
			<tr>


				<td><a href="book_info.php?book_id=<?php echo $book['id'];?>"><img
						src="<?php if(isset($book['cover_picture'])){echo "../".$book['cover_picture'];}?>"
						height=100px width=70px /></a></td>
				<td><a href="book_info.php?book_id=<?php echo $book['id'];?>"><?php echo $book['name'];?></a></td>

				<td><?php echo $book['author'];?></td>
				<td><?php echo $book['published'];?></td>
				<td><?php
			$query = "SELECT * FROM genres WHERE id =" . $book ['genre_id'];
			$genres = $mysqli->query ( $query );
			$genre = $genres->fetch_assoc ();
			echo $genre ['name'];
			?></td>
				<td><?php
			$query = "SELECT * FROM users WHERE id = " . $book ['user_id'];
			$users = $mysqli->query ( $query );
			$user = $users->fetch_assoc ();
			echo $user ['fname'] . ' ' . $user ['lname'];
			?></td>
				<td><div class="btn-group">
						<a class="btn btn-success btn-xs"
							href="update_book.php?book_id=<?php echo $book['id'];?>"><span
							class="glyphicon glyphicon-pencil"></span></a> <a
							href="delete_book.php?book_id=<?php echo $book['id'];?>"
							class="btn btn-danger btn-xs"> <span
							class="glyphicon glyphicon-trash"></span></a>
					</div></td>
			</tr>
			
			<?php
		}
		?>
		
		
		</tbody>
	</table>
	<?php
	$query = "SELECT * FROM books WHERE is_approved = 1";
	$rows = $mysqli->query ( $query )->num_rows;
	$total = ceil ( $rows / $limit );
	?>
	<div align="center">
		<ul class="pagination">
		<?php for($i = 1 ; $i <= $total; $i++){?>
			<li><a href="all_books.php?page=<?php echo $i;?>"
				class="btn btn-primary <?php if($i == $page)echo "active";?>"><?php echo $i;?></a></li>
		<?php }?>
		</ul>
	</div>
</div>