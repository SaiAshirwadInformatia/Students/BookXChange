<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}

if (isset ( $_SESSION ['success_msg'] )) {
	echo '<div class="alert alert-success"><p>Disapproved Successfully</p></div>';
	unset ( $_SESSION ['success_msg'] );
} else if (isset ( $_SESSION ['error_msg'] )) {
	echo '<div class="alert alert-warning"><p>Disapproved failed</p></div>';
	unset ( $_SESSION ['error_msg'] );
}

if (isset ( $_POST ['action'] )) {
	if (isset ( $_POST ['awaiting'] )) {
		if ($_POST ['action'] == "approve") {
			$arr = $_POST ['awaiting'];
			$query = "UPDATE books SET is_approved = 1 WHERE id IN (" . implode ( ",", $arr ) . ")";
			if ($mysqli->query ( $query )) {
				echo '<div class="alert alert-success"><p>Approved successfully.</p></div>';
				foreach ( $arr as $book_id ) {
					$query1 = "SELECT * FROM books WHERE id = " . $book_id;
					$book_info = $mysqli->query ( $query1 );
					$book_info = $book_info->fetch_assoc ();
					
					$query1 = "SELECT * FROM users WHERE id = " . $book_info ['user_id'];
					$user_info = $mysqli->query ( $query1 );
					$user_info = $user_info->fetch_assoc ();
					
					$mail->addAddress ( $user_info ['email'], $user_info ['fname'] . ' ' . $user_info ['lname'] );
					$message = file_get_contents ( '../templates/approved_book.txt' );
					$message = str_replace ( "*|FNAME|*", $user_info ['fname'], $message );
					$message = str_replace ( "*|BOOK_NAME|*", $book_info ['name'], $message );
					$message = str_replace ( "*|BOOK_ID|*", $book_info ['id'], $message );
					$message = nl2br ( $message );
					$mail->msgHTML ( $message );
					$mail->Subject = "Your book successfully approve - BookXchange";
					if ($mail->send ()) {
					} else {
						echo $mail->ErrorInfo;
					}
				}
			} else {
				echo '<div class="alert alert-danger"><p>Failed to Approved</p></div>';
			}
		} else {
			$arr = $_POST ['awaiting'];
			$_SESSION ['delete_book'] = $arr;
			header ( "Location: delete_book.php" );
		}
	}
}
$start = 0;
$limit = 5;
if ($_GET ['page']) {
	$page = $_GET ['page'];
	$start = ($page - 1) * $limit;
}

if (isset ( $_POST ['search'] ) and ! empty ( $_POST ['search'] ) == true) {
	$query = "SELECT * FROM books WHERE (name like '%" . $_POST ['search'] . "%' OR author like '%" . $_POST ['search'] . "%' OR published like '%" . $_POST ['search'] . "%')
			AND is_approved = 0 LIMIT $start , $limit";
	$books = $mysqli->query ( $query );
} else {
	$query = "SELECT * FROM books WHERE is_approved =0 LIMIT $start , $limit";
	$books = $mysqli->query ( $query );
}

require_once 'inc_nav.php';

require_once 'inc_header.php';

if ($books and $books->num_rows > 0) {
	?>

<form action="" method="POST">
	<div class="col-md-9">
		<div class="col-md-6"></div>
		<div class="col-md-4">
			<input type="text" class="form-control" name="search"
				placeholder="search ny fname/lname"
				value="<?php
	if (isset ( $_POST ['search'] ) and ! empty ( $_POST ['search'] ) == true) {
		echo $_POST ['search'];
	}
	?>" />
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-primary" style="margin: -5px">Search</button>
		</div>
		<div class="col-md-12">
			<button type="submit" name="action" value="approve"
				class="btn btn-success">Approve</button>
			<button type="submit" name="action" value="disapprove"
				class="btn btn-danger">Disapprove</button>
		</div>
		<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<th>Name</th>
					<th>Author</th>
					<th>Publisher</th>
					<th>Genre</th>
					<th>Added By</th>
				</tr>
			</thead>
			<tbody>
		<?php while (($book = $books->fetch_assoc()) != null){?>
			<tr>
					<td align=center><input type="checkbox" name="awaiting[]"
						value="<?php echo $book['id'];?>" /></td>
					<td><a href="book_info.php?book_id=<?php echo $book['id'];?>"><?php echo $book['name'];?></a></td>
					<td><?php echo $book['author'];?></td>
					<td><?php echo $book['published'];?></td>
					<td><?php
		$query = "SELECT * FROM genres WHERE id =" . $book ['genre_id'];
		$genres = $mysqli->query ( $query );
		$genre = $genres->fetch_assoc ();
		echo $genre ['name'];
		?></td>
					<td><?php
		$query = "SELECT * FROM users WHERE id = " . $book ['user_id'];
		$users = $mysqli->query ( $query );
		$user = $users->fetch_assoc ();
		echo $user ['fname'] . ' ' . $user ['lname'];
		?></td>			
			<?php }	?>
			
			
			
			
			
			
			
			
			
			
			</tbody>
		</table>
		<?php
	$query = "SELECT * FROM books WHERE is_approved = 0";
	$rows = $mysqli->query ( $query )->num_rows;
	$total = ceil ( $rows / $limit );
	?>

		<div class="col-md-12">
			<button type="submit" name="action" value="approve"
				class="btn btn-success">Approve</button>
			<button type="submit" name="action" value="disapprove"
				class="btn btn-danger">Disapprove</button>
		</div>
		<div class="col-md-12" align="center">
			<ul class="pagination">
		<?php
	for($i = 1; $i <= $total; $i ++) {
		?>
			
			<li><a href="awaiting_books.php?page=<?php echo $i;?>"
					class="btn btn-primary <?php if($i == $page)echo "active";?>"><?php echo $i;?>
				</a></li>
		<?php }?>
		</ul>
		</div>
			
	<?php
} else {
	
	echo '<div class="alert alert-danger col-md-9"><p>There are no awatings books</p></div>';
}
?>


</div>
</form>
