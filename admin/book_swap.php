<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
}


$start = 0;
$limit = 5;
if ($_GET ['page']) {
	$page = $_GET ['page'];
	$start = ($page - 1) * $limit;
}
$query = "SELECT * FROM swap WHERE is_approved = 1 LIMIT $start, $limit";
$swaps = $mysqli->query ( $query );
if ($_GET ['page']) {
	$i = 1 * $page;
} else {
	$i = 1;
}

require_once 'inc_header.php';

require_once 'inc_nav.php';
?>

<div class="col-md-9">
	<table class="table table-striped table-hover ">
		<thead>
			<tr>
				<th>#</th>
				<th>Book Name</th>
				<th>User Name</th>
				<th>Swapped Book Name</th>
				<th>Swapped User Name</th>
				<th>Swap Time</th>
			</tr>
		</thead>
		<tbody>
		<?php while (($swap = $swaps->fetch_assoc()) != null){?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php
			$query = "SELECT * FROM books Where id =" . $swap ['book_id'];
			$books = $mysqli->query ( $query );
			$book = $books->fetch_assoc ();
			?> <img src="<?php echo "../".$book['cover_picture'];?>" height=25px
					width=25px> <?php
			
			echo $book ['name'];
			?></td>
				<td><?php
			$query = "SELECT * FROM users WHERE id = " . $swap ['user_id'];
			$users = $mysqli->query ( $query );
			$user = $users->fetch_assoc ();
			?> <img src="<?php echo "../".$user['display_picture'];?>"
					height=25px width=25px> <?php
			
			echo $user ['fname'] . ' ' . $user ['lname'];
			?></td>
				<td><?php
			$query = "SELECT * FROM books WHERE id = " . $swap ['swapped_book_id'];
			$swapped_book_ids = $mysqli->query ( $query );
			$swapped_book_id = $swapped_book_ids->fetch_assoc ();
			?> <img src="<?php echo "../".$swapped_book_id['cover_picture'];?>"
					height=25px width=25px> <?php
			
			echo $swapped_book_id ['name'];
			?></td>
				<td><?php
			$query = "SELECT * FROM users WHERE id = " . $swap ['swapped_user_id'];
			$swapped_user_ids = $mysqli->query ( $query );
			$swapped_user_id = $swapped_user_ids->fetch_assoc ();
			?> <img
					src="<?php echo "../".$swapped_user_id['display_picture'];?>"
					height=25px width=25px> <?php
			
			echo $swapped_user_id ['fname'] . ' ' . $swapped_user_id ['lname'];
			
			?></td>
				<td><b><?php echo $swap['creation_ts'];?></b></td>
			</tr>
			<?php
			$i ++;
		}
		?>
		</tbody>
	</table>
<?php
$query = "SELECT * FROM swap WHERE is_approved = 1";
$rows = $mysqli->query ( query )->num_rows;
$total = ceil ( $rows / $limit );
?>
<div align="center">
		<ul class="pagination">
	<?php for($i = 1; $i <= $total ; $i++){?>
		<li><a href="book_swap.php?page=<?php echo $i;?>"
				class="btn btn-primary <?php if($_GET['page'])echo "active";?>"><?php echo $i;?></a></li>
	<?php }?>
	</ul>
	</div>
</div>
