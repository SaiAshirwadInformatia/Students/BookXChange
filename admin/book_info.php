<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}

if (isset ( $_GET ['book_id'] )) {
	$query = "SELECT * FROM books WHERE id = " . $_GET ['book_id'];
	$book = $mysqli->query ( $query );
	$book = $book->fetch_assoc ();
	
	$query = "SELECT * FROM users WHERE id = " . $book ['user_id'];
	$user = $mysqli->query ( $query );
	$user = $user->fetch_assoc ();
	
	$query = "SELECT * FROM genres WHERE id = " . $book ['genre_id'];
	$genre = $mysqli->query ( $query );
	$genre = $genre->fetch_assoc ();
	if ($book ['is_approved'] == 0) {
		if (isset ( $_POST ['approve'] )) {
			$query = "UPDATE books SET is_approved = 1 WHERE id =" . $_GET ['book_id'];
			if ($mysqli->query ( $query )) {
				echo '<div class="alert alert-success"><p>Book Approved successfully</p></div>';
				$mail->addAddress ( $user ['email'], $user ['fname'] . ' ' . $user ['lname'] );
				$message = file_get_contents ( '../templates/approved_book.txt' );
				$message = str_replace ( "*|FNAME|*", $user ['fname'], $message );
				$message = str_replace ( "*|BOOK_NAME|*", $book ['name'], $message );
				$message = nl2br ( $message );
				$mail->msgHTML ( $message );
				$mail->Subject = "Your book successfully approve - BookXchange";
				if ($mail->send ()) {
				} else {
					echo $mail->ErrorInfo;
				}
				header ( "Location: awaiting_books.php" );
				exit ();
			}
		} else if (isset ( $_POST ['disapprove'] )) {
			$query = "DELETE FROM books WHERE id = " . $_GET ['book_id'];
			if ($mysqli->query ( $query )) {
				$mail->addAddress ( $user ['email'], $user ['fname'] . ' ' . $user ['lname'] );
				$message = file_get_contents ( '../templates/disapproved_book.txt' );
				$message = str_replace ( "*|FNAME|*", $user ['fname'], $message );
				$message = str_replace ( "*|BOOK_NAME|*", $book ['name'], $message );
				$message = nl2br ( $message );
				$mail->msgHTML ( $message );
				$mail->Subject = "Your book was deleted - BookXchange";
				if ($mail->send ()) {
				} else {
					echo $mail->ErrorInfo;
				}
				header ( "Location: awaiting_books.php" );
				exit ();
			} else {
				echo "Book deletion is failed";
			}
		}
	}
} else {
	header ( "Location: index.php" );
	exit ();
}

require_once 'inc_header.php';

require_once 'inc_nav.php';
?>
<div class="col-md-9">

	<?php if($book['is_approved'] == 0){?>
	<form action="" method="POST">
		<button class="btn btn-success pull-right btn-sm" name="approve">Approve</button>
		<button class="btn btn-danger pull-right btn-sm"
			style="margin-right: 10px" name="disapprove">Disapprove</button>
	</form>
	<?php }?> <a href="update_book.php?book_id=<?php echo $book['id'];?>"><span
		class="btn btn-success btn-sm pull-right glyphicon glyphicon-pencil"
		style="margin-right: 10px"></span></a>
	<hr>
	<div class="col-md-3">
		<img
			src="<?php if(isset($book['cover_picture'])){echo "../".$book['cover_picture'];}?>"
			height=220px width=170px />
	</div>
	<div class="col-md-9">
		<table>
			<tr>
				<td><b>Book Name :</b></td>
				<td><?php echo $book['name'];?></td>
			</tr>
			<tr>
				<td><b>Book Added By :</b></td>
				<td><a href="user_info.php?user_id=<?php echo $user['id'];?>"><?php echo $user['fname'].' '.$user['lname'];?></a></td>
			</tr>
			<tr>
				<td><b>Author :</b></td>
				<td><?php echo $book['author'];?></td>
			</tr>
			<tr>
				<td><b>Publisher :</b></td>
				<td><?php echo $book['published'];?></td>
			</tr>
			<tr>
				<td><b>Published Date :</b></td>
				<td><?php echo $book['published_year'];?></td>
			</tr>
			<tr>
				<td><b>Price :</b></td>
				<td><?php echo $book['price'];?></td>
			</tr>
			<tr>
				<td><b>Genre :</b></td>
				<td><?php echo $genre['name'];?></td>
			</tr>
			<tr>
				<td><b>Pages :</b></td>
				<td><?php echo $book['pages'];?></td>
			</tr>
			<tr>
				<td><b>ISBN 10 :</b></td>
				<td><?php echo $book['isbn10'];?></td>
			</tr>
			<tr>
				<td><b>ISBN 13 :</b></td>
				<td><?php echo $book['isbn13'];?></td>
			</tr>
			<tr>
				<td><b>Language :</b></td>
				<td><?php echo $book['language'];?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-12">
		<b>Description :</b>
		<div class="thumbnail" style="height: relative" name="description">
			<?php echo $book['description'];?>
		</div>
	</div>
</div>