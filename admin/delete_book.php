<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}
if (isset ( $_GET ['book_id'] )) {
	if (isset ( $_POST ['confirmDelete'] )) {
		if ($_POST ['confirmDelete'] == 'Yes') {
			$query = "DELETE FROM books WHERE id = " . $_GET ['book_id'];
			if ($mysqli->query ( $query )) {
				$_SESSION ['success_msg'] = "Book Deleted Successfully";
				header ( "Location: all_books.php" );
				exit ();
			}
		} else {
			$_SESSION ['error_msg'] = "Book Deletion was cancelled";
			header ( "Location: all_books.php" );
			exit ();
		}
	} else {
		
		require_once 'inc_header.php';
		?>
<h2>Delete Book</h2>
<hr>
<form action="" method="POST">
	<div class="form-group">
		<label>Are you sure to delete this book?</label>
		<button type="submit" name="confirmDelete" value="Yes"
			class="btn btn-danger">Yes</button>
		<button type="submit" name="confirmDelete" value="No"
			class="btn btn-success">No</button>
	</div>
</form>
<?php
	}
} else {
	$arr = $_SESSION ['delete_book'];
	if (isset ( $_POST ['confirmDelete'] )) {
		if ($_POST ['confirmDelete'] == 'Yes') {
			$query = "DELETE FROM books WHERE id IN(" . implode ( $arr ) . ")";
			if ($mysqli->query ( $query )) {
				$_SESSION ['success_msg'] = "Book Deleted Successfully";
				unset ( $_SESSION ['delete_book'] );
				foreach ( $arr as $book_id ) {
					$query1 = "SELECT * FROM books WHERE id = " . $book_id;
					$book_info = $mysqli->query ( $query1 );
					$book_info = $book_info->fetch_assoc ();
					
					$query1 = "SELECT * FROM users WHERE id = " . $book_info ['user_id'];
					$user_info = $mysqli->query ( $query1 );
					$user_info = $user_info->fetch_assoc ();
					
					$mail->addAddress ( $user_info ['email'], $user_info ['fname'] . ' ' . $user_info ['lname'] );
					$message = file_get_contents ( '../templates/disapproved_book.txt' );
					$message = str_replace ( "*|FNAME|*", $user_info ['fname'], $message );
					$message = str_replace ( "*|BOOK_NAME|*", $book_info ['name'], $message );
					$message = str_replace ( "*|BOOK_ID|*", $book_info ['id'], $message );
					$message = nl2br ( $message );
					$mail->msgHTML ( $message );
					$mail->Subject = "Your book was deleted - BookXchange";
					if ($mail->send ()) {
					} else {
						echo $mail->ErrorInfo;
					}
				}
				header ( "Location: awaiting_books.php" );
				exit ();
			}
		} else {
			$_SESSION ['error_msg'] = "Book Deletion was cancelled";
			unset ( $_SESSION ['delete_book'] );
			header ( "Location: awaiting_books.php" );
			exit ();
		}
	} else {
		require_once 'inc_header.php';
		?>
<h2>Delete Book</h2>
<hr>
<form action="" method="POST">
	<div class="form-group">
		<label>Are you sure to delete this book?</label>
		<button type="submit" name="confirmDelete" value="Yes"
			class="btn btn-danger">Yes</button>
		<button type="submit" name="confirmDelete" value="No"
			class="btn btn-success">No</button>
	</div>
</form>
<?php
	}
}
?>