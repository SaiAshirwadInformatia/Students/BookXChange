<?php

require_once 'db_connect.php';

if ((! isset ( $_SESSION ['admin'] ) and ! isset ( $_SESSION ['delete_swap'] )) or (empty ( $_SESSION ['admin'] ) == true and ! isset ( $_SESSION ['delete_swap'] ))) {
	header ( "Location: inc_header.php" );
	exit ();
}

if (isset ( $_POST ['confirmDelete'] )) {
	$arr = $_SESSION['delete_swap'];
	if ($_POST ['confirmDelete'] == 'Yes') {
		$query = "DELETE FROM swap WHERE id IN(" . implode ( $arr ) . ")";
		if ($mysqli->query ( $query )) {
			$_SESSION ['success_msg'] = "Swap Deleted Successfully";
			unset ( $_SESSION ['delete_swap'] );
			header ( "Location: awaiting_swap.php" );
			exit ();
		}
	} else {
		$_SESSION ['error_msg'] = "Swap Deletion was cancelled";
		unset ( $_SESSION ['delete_swap'] );
		header ( "Location: awaiting_swap.php" );
		exit ();
	}
} else {
	require_once 'inc_header.php';
	?>
<h2>Delete Swap</h2>
<hr>
<form action="" method="POST">
	<div class="form-group">
		<label>Are you sure to delete this swap?</label>
		<button type="submit" name="confirmDelete" value="Yes"
			class="btn btn-danger">Yes</button>
		<button type="submit" name="confirmDelete" value="No"
			class="btn btn-success">No</button>
	</div>
</form>
<?php
}
?>