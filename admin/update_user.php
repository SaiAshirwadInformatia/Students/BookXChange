<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}

$user_id = $_GET ['user_id'];
$query = "SELECT * FROM users WHERE id = $user_id";
$users = $mysqli->query ( $query );
$user = $users->fetch_assoc ();

require_once 'inc_header.php';
$fields = array (
		"fname",
		"lname",
		"phone",
		"gender",
		"blood_group",
		"dob",
		"display_picture" 
);
$fields2 = array (
		"address1",
		"address2",
		"country",
		"state",
		"city",
		"pincode",
		"landmark" 
);

$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
$user_addresses = $mysqli->query ( $query );
$user_address = $user_addresses->fetch_assoc ();

$user_id = $user ['id'];
if (isset ( $_POST ) and count ( $_POST ) > 0) {
	$query = "SELECT * FROM users_addresses";
	$ids = $mysqli->query ( $query );
	if ($ids->num_rows > 0) {
		$query = "SELECT * FROM users_addresses WHERE user_id = $user_id";
		$rows = $mysqli->query ( $query );
		if (isset ( $rows ) and $rows->num_rows > 0) {
			
			$set_array2 = array ();
			$query = "UPDATE users_addresses SET ";
			foreach ( $fields2 as $field2 ) {
				if (isset ( $_POST ) and ! empty ( $_POST )) {
					$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
				}
			}
			$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
			if ($mysqli->query ( $query ) === TRUE) {
				$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
				$user_addresses = $mysqli->query ( $query );
				$user_address = $user_addresses->fetch_assoc ();
			}
		} else {
			$query = "INSERT INTO users_addresses (user_id) VALUES ($user_id)";
			if ($mysqli->query ( $query )) {
				$set_array2 = array ();
				$query = "UPDATE users_addresses SET ";
				foreach ( $fields2 as $field2 ) {
					if (isset ( $_POST ) and ! empty ( $_POST )) {
						$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
					}
				}
				$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
				if ($mysqli->query ( $query ) === TRUE) {
					$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
					$user_addresses = $mysqli->query ( $query );
					$user_address = $user_addresses->fetch_assoc ();
				}
			}
		}
	} else {
		$query = "INSERT INTO users_addresses (user_id) VALUES ($user_id)";
		if ($mysqli->query ( $query )) {
			$set_array2 = array ();
			$query = "UPDATE users_addresses SET ";
			foreach ( $fields2 as $field2 ) {
				if (isset ( $_POST ) and ! empty ( $_POST )) {
					$set_array2 [] = "$field2 = '" . $_POST [$field2] . "'";
				}
			}
			$query .= implode ( ",", $set_array2 ) . "WHERE user_id =" . $user ['id'];
			if ($mysqli->query ( $query ) === TRUE) {
				$query = "SELECT * FROM users_addresses WHERE user_id =" . $user ['id'];
				$user_addresses = $mysqli->query ( $query );
				$user_address = $user_addresses->fetch_assoc ();
			}
		} else {
			echo "User ID didn't passed in user_addresses";
		}
	}
	
	$set_array = array ();
	$query = "UPDATE users
            SET ";
	foreach ( $fields as $field ) {
		if ($field == "display_picture") {
			if (isset ( $_FILES ['display_picture'] ) and ! empty ( $_FILES ['display_picture'] ) == true) {
				if ($_FILES ['display_picture'] ['error'] == 0) {
					$move = "upload/" . md5 ( $_FILES ['display_picture'] ['name'] );
					$_POST ['display_picture'] = $move;
					$set_array [] = "$field = '" . $_POST [$field] . "'";
					$move = "../" . $move;
					move_uploaded_file ( $_FILES ['display_picture'] ['tmp_name'], $move );
					$query1 = "SELECT * FROM users WHERE id = " . $_GET ['user_id'];
					$rm_user = $mysqli->query ( $query1 );
					$rm_user = $rm_user->fetch_assoc ();
					if (isset ( $rm_user ['display_picture'] ) and file_exists ( "../" . $rm_user ['display_picture'] )) {
						$rm_user ['display_picture'] = "../" . $rm_user ['display_picture'];
						if (! unlink ( $rm_user ['display_picture'] )) {
							echo "Display picture Deletion failed";
						}
					}
				}
			} else {
				continue;
			}
		} else if (isset ( $_POST [$field] ) and ! empty ( $_POST [$field] )) {
			$set_array [] = "$field = '" . $_POST [$field] . "'";
		}
	}
	$query .= implode ( ",", $set_array ) . " WHERE id = " . $user ['id'];
	if ($mysqli->query ( $query ) === TRUE) {
		$query = "SELECT * FROM users WHERE id = " . $user ['id'];
		$users = $mysqli->query ( $query );
		$user = $users->fetch_assoc ();
		echo '<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Your Profile Updated Successfully</strong>
</div>';
	} else {
		echo "Error updating record: " . $mysqli->error;
	}
}

require_once 'inc_header.php';
require_once 'inc_nav.php';

?>
<div class="col-md-9">
	<form class="form-horizontal" action="" method="POST"
		enctype="multipart/form-data">
		<fieldset>
			<legend>Personal Information</legend>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="fname">First Name </label> <input type="text"
					name="fname" class="form-control"
					value="<?php echo $user["fname"] ?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="lname">Last Name</label> <input type="text"
					class="form-control" name="lname"
					value="<?php echo $user["lname"]?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="username">Username</label> <input type="text"
					class="form-control" name="username"
					value="<?php echo $user["username"]?>" disabled />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="email">Email ID</label> <input type="email"
					class="form-control" name="email"
					value="<?php echo $user["email"]?>" disabled />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="phone">Phone No.</label> <input type="number"
					class="form-control" name="phone"
					value="<?php echo $user["phone"]?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="dob">Date of Birth</label> <input type="date"
					placeholder="yyyy-mm-dd" class="form-control" name="dob"
					value="<?php if($user["dob"] == '0000-00-00'){echo '';}else {echo $user["dob"];}?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label for="gender">Gender</label>
				<div class="radio">
					<?php if($user["gender"] == "m"){?>
						<label> <input type="radio" name="gender" value="m" checked />
						Male
					</label> <label> <input type="radio" name="gender" value="f" />
						Female
					</label>
						<?php }else{?>
						<label> <input type="radio" name="gender" value="m" /> Male
					</label> <label> <input type="radio" name="gender" value="f"
						checked /> Female
					</label><?php }?>
					</div>
			</div>
			<?php
			$blood_group = array (
					"A+",
					"A-",
					"B+",
					"B-",
					"O+",
					"O-",
					"AB+",
					"AB-" 
			);
			?>
			
			
			<div class="col-md-4" style="padding-top: 15px">
				<label>Blood Group</label> <select name="blood_group" id="select"
					class="form-control">
					<option>Select</option>
					<?php
					foreach ( $blood_group as $blood_group ) {
						echo '<option value="' . $blood_group . '"' . (($user ['blood_group'] == $blood_group) ? ' selected' : '') . '>' . $blood_group . '</option>';
					}
					?>
				</select>
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Profile Picture</label> <input type="file"
					name="display_picture" class="form-control" />
			</div>
		</fieldset>
		<fieldset>
			<legend>Address</legend>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Address1</label> <input type="text" name="address1"
					class="form-control"
					value="<?php echo isset($user_address['address1'])?$user_address['address1']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Address2</label> <input type="text" class="form-control"
					name="address2"
					value="<?php echo isset($user_address['address2'])?$user_address['address2']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>City</label> <input type="text" class="form-control"
					name="city"
					value="<?php echo isset($user_address['city'])?$user_address['city']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>State</label> <input type="text" class="form-control"
					name="state"
					value="<?php echo isset($user_address['state'])?$user_address['state']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Country</label> <input type="text" class="form-control"
					name="country"
					value="<?php echo isset($user_address['country'])?$user_address['country']:' ';?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Pincode</label> <input type="number" class="form-control"
					name="pincode"
					value="<?php  if($user_address['pincode'] == 0){echo '';}else{echo $user_address['pincode'];}?>" />
			</div>
			<div class="col-md-4" style="padding-top: 15px">
				<label>Landmark</label> <input type="text" class="form-control"
					name="landmark"
					value="<?php echo isset($user_address['landmark'])?$user_address['landmark']:' ';?>" />
			</div>
		</fieldset>
		<div class="row" style="padding: 15px" align="center">
			<button class="btn btn-primary" type="submit">Save Changes</button>
		</div>
	</form>
</div>