<?php

if (! isset($_SESSION['admin']) or empty($_SESSION['admin']) == true) {
    header("Location: inc_header.php");
    exit();
}

require_once 'inc_header.php';

?>
<style>
.display {
	list-style-type: none;
	display: none;
	display: none;
}

#book, #book+ul, #right, #user, #user+ul, #dash {
padding: 5px;
background-color: #e5eecc;
border: solid 1px #c3c3c3;
}
</style>
<div class="container-fluid" style="padding: 10px">
	<div class="row">
		<div class="col-md-3">
			<ul class="nav nav-pills nav-stacked ">
				<li role="presentation" class="active" id="dash"><a href="index.php">Dashboard</a></li>
				<li role="presentation" id="user" class="active"><a>Users</a></li>


				<ul>
					<li class="display user_list"><a href="all_users.php">All Users</a></li>
					<li class="display user_list"><a href="user_create.php">Create User</a></li>
				</ul>

				<li role="presentation" id="book" class="active"><a>Books</a></li>
				<ul>
					<li class="display book_list"><a href="all_books.php">All Books</a></li>
					<li class="display book_list"><a href="book_create.php">Create Book</a></li>
					<li class="display book_list"><a href="awaiting_books.php">Awaiting
							Books</a></li>
					<li class="display book_list"><a href="book_swap.php">Swap Book</a></li>
					<li class="display book_list"><a href="awaiting_swap.php">Awaiting
							Swap</a></li>
				</ul>
			</ul>
		</div>
		<script> 
$(document).ready(function(){
  $("#user").click(function(){
    $(".user_list").slideToggle("200");
  });
  $("#book").click(function(){
	    $(".book_list").slideToggle("200");
	    	});
	  

  
});
</script>
