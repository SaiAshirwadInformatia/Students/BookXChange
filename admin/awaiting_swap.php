<?php

require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}


if (isset ( $_SESSION ['success_msg'] )) {
	echo '<div class="alert alert-success"><p>Disapproved Successfully</p></div>';
	unset ( $_SESSION ['success_msg'] );
} else if (isset ( $_SESSION ['error_msg'] )) {
	echo '<div class="alert alert-warning"><p>Disapproved failed</p></div>';
	unset ( $_SESSION ['error_msg'] );
}

if (isset ( $_POST ['action'] )) {
	if (isset ( $_POST ['awaiting'] )) {
		$arr = $_POST ['awaiting'];
		if ($_POST ['action'] == "approve") {
			$query = "UPDATE swap SET is_approved = 1 WHERE id in (" . implode ( ",", $arr ) . ")";
			if ($mysqli->query ( $query )) {
				echo '<div class="alert alert-success"><p>Approved Successfully</p></div>';
			} else {
				echo '<div class="alert alert-danger"><p>Approved Failed</p></div>';
			}
		} else if ($_POST ['action'] == "dis_approve") {
			$_SESSION ['delete_swap'] = $arr;
			header ( "Location: delete_swap.php" );
		}
	}
}
$start = 0;
$limit = 5;
if ($_GET ['page']) {
	$page = $_GET ['page'];
	$start = ($page - 1) * $limit;
}
$query = "SELECT * FROM swap  WHERE is_approved = 0 LIMIT $start,$limit";
$swap_books = $mysqli->query ( $query );

require_once 'inc_header.php';

require_once 'inc_nav.php';
?>

<div class="col-md-9">
	<form action="" method="POST">
		<!--  <button type="submit" name="action" value="approve"
			class="btn btn-success">Approve</button> -->
		<button type="submit" name="action" value="dis_approve"
			class="btn btn-danger">Disapprove</button>
		<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<th></th>
					<th>Book Name</th>
					<th>User Name</th>
					<th>Swapped Book Name</th>
					<th>Swapped User Name</th>
					<th>Swap Time</th>
				</tr>
			</thead>
			<tbody>
		<?php while (($swap_book = $swap_books->fetch_assoc()) != null){?>
			<tr>
					<td><input type="checkbox" name="awaiting[]"
						value=<?php echo $swap_book['id'];?> /></td>
					<td><?php
			$query = "SELECT * FROM books Where id =" . $swap_book ['book_id'];
			$books = $mysqli->query ( $query );
			$book = $books->fetch_assoc ();
			?><a href="book_info.php?book_id=<?php echo $book['id']?>"><img
							src="<?php echo "../".$book['cover_picture'];?>" height=25px
							width=25px><?php
			
echo $book ['name'];
			?></a></td>
					<td><?php
			$query = "SELECT * FROM users WHERE id = " . $swap_book ['user_id'];
			$users = $mysqli->query ( $query );
			$user = $users->fetch_assoc ();
			?><a href="user_info.php?user_id=<?php echo $user['id']?>"><img
							src="<?php echo "../".$user['display_picture'];?>" height=25px
							width=25px><?php
			
echo $user ['fname'] . ' ' . $user ['lname'];
			?></a></td>
					<td><?php
			$query = "SELECT * FROM books WHERE id = " . $swap_book ['swapped_book_id'];
			$swapped_book_ids = $mysqli->query ( $query );
			$swapped_book_id = $swapped_book_ids->fetch_assoc ();
			?><a
						href="book_info.php?book_id=<?php echo $swapped_book_id['id']?>"><img
							src="<?php echo "../".$swapped_book_id['cover_picture'];?>"
							height=25px width=25px><?php
			
echo $swapped_book_id ['name'];
			?></a></td>
					<td><?php
			$query = "SELECT * FROM users WHERE id = " . $swap_book ['swapped_user_id'];
			$swapped_user_ids = $mysqli->query ( $query );
			$swapped_user_id = $swapped_user_ids->fetch_assoc ();
			?><a
						href="user_info.php?user_id=<?php echo $swapped_user_id['id']?>"><img
							src="<?php echo "../".$swapped_user_id['display_picture'];?>"
							height=25px width=25px><?php
			
echo $swapped_user_id ['fname'] . ' ' . $swapped_user_id ['lname'];
			
			?></a></td>
					<td><b><?php echo $swap_book['creation_ts'];?></b></td>
				</tr>
				
			<?php }?>
			</tbody>
		</table>
		<!--  <button type="submit" name="action" value="approve"
			class="btn btn-success">Approve</button> -->
		<button type="submit" name="action" value="dis_approve"
			class="btn btn-danger">Disapprove</button>

<?php
$query = "SELECT * FROM swap WHERE is_approved = 0";
$rows = $mysqli->query ( $query )->num_rows;
$total = ceil ( $rows / $limit );
?>
<div align="center">
			<ul class="pagination">
	<?php for($i = 1; $i <= $total ; $i++){?>
		<li><a href="awaiting_swap.php?page=<?php echo $i;?>"
					class="btn btn-primary <?php if($_GET['page'])echo "active";?>"><?php echo $i;?></a></li>
	<?php }?>
	</ul>
		</div>
	</form>
</div>
