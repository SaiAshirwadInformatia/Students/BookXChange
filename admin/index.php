<?php

require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) and empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}


$query = "SELECT COUNT(*) AS user_count FROM users";
$total_user = $mysqli->query ( $query );
$total_user = $total_user->fetch_assoc ();

$query = "SELECT COUNT(*) AS user_count FROM users WHERE gender = 'm' ";
$male_user = $mysqli->query ( $query );
$male_user = $male_user->fetch_assoc ();

$query = "SELECT COUNT(*) AS user_count FROM users WHERE gender = 'f'";
$female_user = $mysqli->query ( $query );
$female_user = $female_user->fetch_assoc ();

$query = "SELECT COUNT(*) AS book_count FROM books";
$total_book = $mysqli->query ( $query );
$total_book = $total_book->fetch_assoc ();

$query = "SELECT COUNT(*) AS book_count FROM books WHERE is_approved = 1";
$approved_book = $mysqli->query ( $query );
$approved_book = $approved_book->fetch_assoc ();

$query = "SELECT COUNT(*) AS book_count FROM swap";
$swap_book = $mysqli->query ( $query );
$swap_book = $swap_book->fetch_assoc ();

$not_swapped_book = $approved_book ['book_count'] - ($swap_book ['book_count'] * 2);

$query = "SELECT COUNT(*) AS book_count FROM books WHERE is_approved = 0";
$pending_approval = $mysqli->query ( $query );
$pending_approval = $pending_approval->fetch_assoc ();

$query = "SELECT COUNT(*)AS page_count, page FROM hits GROUP BY page ORDER BY count(page) desc LIMIT 0, 3";
$page_hits = $mysqli->query ( $query );


require_once 'inc_header.php';

require_once 'inc_nav.php';
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<?php
// It is for Number of Books In which condition
?>
<script>
google.load('visualization', '1.0', {'packages':['corechart']});

google.setOnLoadCallback(drawChart);


function drawChart(){


	 var data = new google.visualization.DataTable();
     data.addColumn('string', 'Books');
     data.addColumn('number', 'Count');
     data.addRows([
       ['Swapped Books', <?php echo $swap_book['book_count'] * 2;?>],
       ['Books Not Swapped', <?php echo $not_swapped_book;?>],
       ['Books Not Approved', <?php echo $pending_approval['book_count'];?>],
     ]);


     var options = {'title':'All Books',
             'width':350,
             'height':350};
var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
chart.draw(data, options);
}


<?php
// It is for find number of Male And female
?>
    
</script>

<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ["Gender", "Count", { role: "style" } ],
    ["Male", <?php echo $male_user['user_count'];?>, "#FF9900"],
    ["Female",<?php echo $female_user['user_count'];?>, "silver"]
  ]);

  var view = new google.visualization.DataView(data);
  view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" },
                   2]);

  var options = {
    title: "Chart",
    width: 350,
    height: 350,
    bar: {groupWidth: "50%"},
    legend: { position: "none" },
  };
  var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
  chart.draw(view, options);
}
</script>



<div class="col-md-9">
	<div class="col-md-4">
		<div class=" panel panel-info">
			<div class="panel-heading">
				<b>Total user</b>
			</div>
			<div class="panel-body" align=center>
				<span class="glyphicon glyphicon-user icon_size"></span><span
					class="font_size">
					<?php echo ' '.$total_user['user_count'];?></span>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class=" panel panel-info">
			<div class="panel-heading">
				<b>Total Books</b>
			</div>
			<div class="panel-body" align=center>
				<span class="glyphicon glyphicon-book icon_size"></span><span
					class="font_size"><?php echo ' '.$total_book['book_count'];?></span>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class=" panel panel-info">
			<div class="panel-heading">
				<b>Swapped Books</b>
			</div>
			<div class="panel-body" align=center>
				<span class="glyphicon glyphicon-transfer icon_size"></span><span
					class="font_size"><?php echo ' '.$swap_book['book_count'];?></span>
			</div>
		</div>
	</div>
	<div class="col-md-4" id="columnchart_values"></div>
	<div class="col-md-4" id="chart_div"></div>
	<div class="col-md-4">
		<div class=" panel panel-info">
			<div class="panel-heading">
				<b>Top 3 Pages</b>
			</div>
			<div class="panel-body" align=left>
			<?php
			
			$i = 1;
			while ( ($page_hit = $page_hits->fetch_assoc ()) != null ) {
				?>
				<h4>
					<?php echo $i.' : <a href = "../'.$page_hit['page'].'">'.$page_hit['page'];?><span
						class="badge"><?php echo '&nbsp;&nbsp;&nbsp;' .$page_hit['page_count'] .'</a>'; ?></span>
				</h4>
				<?php $i++;}?>
			</div>
		</div>
	</div>
</div>
<style>
.icon_size {
	font-size: 40px;
	color: #CC6600;
}

.font_size {
	font-size: 30px;
}
</style>