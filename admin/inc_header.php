<?php
if (! isset ( $_SESSION )) {
	session_start ();
}
?>

<!DOCTYPE html>
<!--
----------------------------------------------------------------------------
Developed and Maintained by Sai Ashirwad Informatia
----------------------------------------------------------------------------
----------------------------------------------------------------------------
Authors
----------------------------------------------------------------------------
Akshay Mane         akmane29@gmail.com
Kunal Band          kunalbund1996@gmail.com
----------------------------------------------------------------------------
-->
<html>
<head>
<title>Bookxchange</title>
<link rel="icon" href="../images/book_image1.jpg" type="image/jpg" />
<link rel="stylesheet" href="../style.css" />
<link rel="stylesheet" type="../bower_components/socjs/soc.min.css">
<link rel="stylesheet"
	href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
<link
	href="../bower_components/bootstrap-material-design/dist/css/roboto.min.css"
	rel="stylesheet">
<link
	href="../bower_components/bootstrap-material-design/dist/css/material.min.css"
	rel="stylesheet">
<link
	href="../bower_components/bootstrap-material-design/dist/css/ripples.min.css"
	rel="stylesheet">


<link
	href="../bower_components/bootstrap-material-design/dist/css/material-fullpalette.css"
	rel="stylesheet">
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script
	src="../bower_components/bootstrap-material-design/dist/js/material.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="../bower_components/tinymce/tinymce.min.js"></script>
<script src="http://www.google.com/jsapi"></script>
</head>
<body>
	<script>
            $(function() {
                $.material.init();
            });
        </script>

	<div class="container-fluid header-img header-back-img">
	<?php if(!isset($_SESSION['admin']) or empty($_SESSION['admin']) == true){?>
		<form action="login.php" method="POST">
			<div class="row">
				<div class="col-md-7 col-xs-12">
					<img alt="BookXchange" src="../images/BookXchange.png"
						style="padding-top: 22px;">
				</div>
				<div class="col-md-2 form-group col-xs-7">
					<label class="form-control">Username</label> <input type="text"
						class="form-control" name="username" style="border: 2px solid;" />
				</div>
				<div class="col-md-2 form-group">
					<label class="form-control">Password</label> <input type="password"
						class="form-control" name="password" style="border: 2px solid;" />
				</div>
				<div class="col-md-1" align="center">
					<label><a href="register.php" style="color: black"></a></label>
					<button class="btn btn-success" type="submit"
						style="margin-top: 31px; margin-left: 0px; margin-bottom: 0px; padding: 8px">Login</button>
				</div>
			</div>
		</form>
	</div><?php }else {?>
	<div class="container-fluid header-img header-back-img">
		<div class="col-md-10"></div>
		<b>Welcome <?php echo $_SESSION['loggedadmin'];?> </b><a href="logout.php"
						class="btn btn-danger btn-xs" style="margin-top: 1px;"><span
						class="glyphicon glyphicon-off"></span> Logout</a>
	</div>
<?php } ?>