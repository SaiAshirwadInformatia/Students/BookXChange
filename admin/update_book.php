<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header.php" );
	exit ();
}

$query = "SELECT * FROM genres";
$genres = $mysqli->query ( $query );

$query = "SELECT * FROM users";
$users = $mysqli->query ( $query );

if (isset ( $_GET ['book_id'] ) and ! empty ( $_GET ['book_id'] ) == true) {
	$query = "SELECT * FROM books WHERE id = " . $_GET ['book_id'];
	$books = $mysqli->query ( $query );
	$book = $books->fetch_assoc ();
	
	if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
		$fields = array (
				"name",
				"author",
				"published",
				"price",
				"published_year",
				"language",
				"genre_id",
				"pages",
				"isbn13",
				"isbn10",
				"description",
				"user_id",
				"cover_picture" 
		);
		foreach ( $fields as $field ) {
			if ($field == "is_approved") {
				continue;
			}
			if (isset ( $_POST [$field] ) and ! empty ( $_POST [$field] ) == true) {
				$$field = $_POST [$field];
				$isValid = true;
			} else {
				if ($field != 'cover_picture') {
					$isValid = false;
					break;
				}
			}
		}
		
		if ($isValid) {
			foreach ( $fields as $field ) {
				if ($field == "cover_picture") {
					if (isset ( $_FILES ['cover_picture'] ) and ! empty ( $_FILES ['cover_picture'] ) == true) {
						if ($_FILES ['cover_picture'] ['error'] == 0) {
							$_POST ['cover_picture'] = $move = "upload/book/" . md5 ( $_FILES ['cover_picture'] ['name'] . "." . $book ['id'] );
							$move = '../' . $move;
							move_uploaded_file ( $_FILES ['cover_picture'] ['tmp_name'], $move );
							if (isset ( $_GET ['book_id'] )) {
								$query1 = "SELECT * FROM books WHERE id =" . $_GET ['book_id'];
								$rm_picture = $mysqli->query ( $query1 );
								$rm_picture = $rm_picture->fetch_assoc ();
								if (isset ( $rm_picture ['cover_picture'] )) {
									$rm_picture ['cover_picture'] = "../" . $rm_picture ['cover_picture'];
									if (! unlink ( $rm_picture ['cover_picture'] )) {
										echo "Book image Deletion failed";
									}
								}
							}
							$$field = $_POST ['cover_picture'];
						}
					}
				} elseif ((is_string ( $_POST [$field] ))) {
					$$field = $mysqli->escape_string ( $_POST [$field] );
				} else {
					$$field = $_POST [$field];
				}
				if ($field == "cover_picture") {
					if (! isset ( $_POST ['cover_picture'] ) and empty ( $_POST ['cover_picture'] ) == true) {
						continue;
					} else {
						$arr [] = "$field = '$_POST[$field]'";
					}
				} else {
					$arr [] = "$field = '$_POST[$field]'";
				}
			}
			$query = "UPDATE books SET " . implode ( ",", $arr ) . "WHERE id = " . $_GET ['book_id'];
			
			if ($mysqli->query ( $query )) {
				$query = "SELECT * FROM books WHERE id =" . $_GET ['book_id'];
				$books = $mysqli->query ( $query );
				$book = $books->fetch_assoc ();
				echo "Book Updated Successfully";
			} else {
				echo "Book Failed";
			}
		} else {
			echo "Please Filled all field";
		}
	}
}

require_once 'inc_header.php';
require_once 'inc_nav.php';
?>
<?php

if (isset ( $book ['id'] )) {
	echo '<input type="hidden" name="id" value="' . $book ['id'] . '" />';
}
if (isset ( $book ['is_active'] )) {
	echo '<input type="hidden" name="is_active" value="' . $book ['is_active'] . '" />';
}
if (isset ( $book ['cover_picture'] ) and ! empty ( $book ['cover_picture'] )) {
	echo '<input type="hidden" name="cover_picture" value="' . $book ['cover_picture'] . '" />';
} else {
	echo '<input type="hidden" name="cover_picture" value="images/book_image.jpg" />';
}
?>
<div class="col-md-9">
	<form class="form-horizontal" action="" method="POST"
		enctype="multipart/form-data">
		<fieldset>
			<h4>Enter Information About Book</h4>
			<hr>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Name</label>
				<div class="col-lg-9">
					<input type="hidden" name="user_id" /> <input type="text"
						class=" form-control" name="name" id="inputDefault"
						placeholder="(ex: Lord of Rings)"
						value="<?php echo $book['name'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Author</label>
				<div class="col-lg-9">
					<input type="text" class=" form-control" name="author"
						id="inputDefault" placeholder="(ex: J. R. R. Tolkien)"
						value="<?php echo $book['author'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publisher</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="published"
						placeholder="(ex: George Allen & Unwin"
						value="<?php echo $book['published'];?>" />
				</div>
				<label for="textArea" class="col-md-2 control-label">Price</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="price"
						placeholder="(ex: 230)" value="<?php echo $book['price'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publication
					Date</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="published_year"
						placeholder="(ex: YYYY-MM-DD"
						value="<?php echo $book['published_year'];?>" />
				</div>
				<label for="textArea" class="col-md-2 control-label">Language</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="language"
						placeholder="(ex: English)"
						value="<?php echo $book['language'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">Genre</label>
				<div class="col-md-3">
					<select class="form-control" id="select" name="genre_id">
						<option>--Select--</option>
				    	   	<?php
													while ( ($genre = $genres->fetch_assoc ()) != null ) {
														echo '<option value="' . $genre ['id'] . '"' . (($genre ['id'] == $book ['genre_id']) ? 'selected' : '') . '>' . $genre ['name'] . '</option>';
													}
													?>
					</select>
				</div>
				<label for="inputDefault" class="col-md-2 control-label">Pages</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="pages"
						id="inputEmail" placeholder="(ex: 350)"
						value="<?php echo $book['pages'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">ISBN-13 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn13"
						id="inputPassword" placeholder="(ex: 9780689869914)"
						value="<?php echo $book['isbn13'];?>" />
				</div>
				<label for="inputDefault" class="col-md-2 control-label">ISBN-10 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn10"
						id="inputPassword" placeholder="(ex: 9780689869)"
						value="<?php echo $book['isbn10'];?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Description</label>
				<div class="col-md-9">
					<textarea class="form-control" id="textArea"
						placeholder="Any information about Book condition / features"
						name="description"><?php echo $book['description'];?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Book Image</label>
				<div class="col-md-9">
					<input type="file" name="cover_picture" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Select User</label>
				<div class="col-md-4">
					<select class="form-control" id="select" name="user_id">
						<option>Select</option>
						<?php
						while ( ($user = $users->fetch_assoc ()) != null ) {
							echo '<option value="' . $user ['id'] . '"' . (($user ['id'] == $book ['user_id']) ? 'selected' : '') . '>' . $user ['fname'] . ' ' . $user ['lname'] . '</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-9 col-lg-offset-2">
					<button type="submit" class="btn btn-primary">Add Book</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$(document).ready(function(){
    tinymce
	.init({
	    selector : "textarea",
	    relative_urls : true,
	    remove_script_host : false
	});
});
</script>
