<?php

require_once 'db_connect.php';

if (! isset($_SESSION['admin']) or empty($_SESSION['admin']) == true) {
    header("Location: inc_header.php");
    exit();
}

if (isset($_GET['user_id']) and ! empty($_GET['user_id']) == true) {
    $query = "SELECT * FROM users WHERE id =" . $_GET['user_id'];
    $user = $mysqli->query($query);
    $user = $user->fetch_assoc();
    
    $query = "SELECT * FROM users_addresses WHERE user_id = " . $_GET['user_id'];
    $user_addr = $mysqli->query($query);
    $user_addr = $user_addr->fetch_assoc();
    
    $query = "SELECT * FROM books WHERE user_id = " . $_GET['user_id'] . " AND is_approved = 1";
    $books = $mysqli->query($query);
}

require_once 'inc_header.php';

require_once 'inc_nav.php';
?>
<a href="update_user.php?user_id=<?php echo $user['id'];?>" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
<div class="col-md-9">
	<div class="col-md-3">
		<img
			src="<?php if(isset($user['display_picture'])){echo "../".$user['display_picture'];}?>"
			height=290px width=200px class="img img-thumbnail" />
	</div>
	<div class="col-md-5">
		<table>
			<tr>
				<td><b>Name :</b></td>
				<td><?php echo $user['fname']." ".$user['lname'];?></td>
			</tr>
			<tr>
				<td><b>Email ID :</b></td>
				<td><?php echo $user['email'];?></td>
			</tr>
			<tr>
				<td><b>Contact No. :</b></td>
				<td><?php echo $user['phone'];?></td>
			</tr>
			<tr>
				<td><b>Username :</b></td>
				<td><?php echo $user['username'];?></td>
			</tr>
			<tr>
				<td><b>Gender :</b></td>
				<td><?php if($user['gender'] == "m"){echo "Male";}else{echo "Female";}?></td>
			</tr>
			<tr>
				<td><b>Date of Birth :</b></td>
				<td><?php echo $user['dob'];?></td>
			</tr>
			<tr>
				<td><b>Creation Time :</b></td>
				<td><?php echo $user['creation_ts'];?></td>
			</tr>
			<?php if(isset($user['lastmodified_ts'])and $user['lastmodified_ts'] != null){?>
			<tr>
				<td><b>Lastmodified Time :</b></td>
				<td><?php echo $user['lastmodified_ts'];?></td>
			</tr>
			<?php }?>
		</table>
	</div>
	<div class="col-md-4">
		<table>
			<tr>
				<td><b>Address1 :</b></td>
				<td><?php echo $user_addr['address1'];?></td>
			</tr>
			<tr>
				<td><b>Address2 :</b></td>
				<td><?php echo $user_addr['address2'];?></td>
			</tr>
			<tr>
				<td><b>City :</b></td>
				<td><?php echo $user_addr['city'];?></td>
			</tr>
			<tr>
				<td><b>State :</b></td>
				<td><?php echo $user_addr['state'];?></td>
			</tr>
			<tr>
				<td><b>Country :</b></td>
				<td><?php echo $user_addr['country'];?></td>
			</tr>
			<tr>
				<td><b>Pincode :</b></td>
				<td><?php echo $user_addr['pincode'];?></td>
			</tr>
			<tr>
				<td><b>Landmark :</b></td>
				<td><?php echo $user_addr['landmark'];?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-12">
		<h4>Books Added By <?php echo $user['fname']." ".$user['lname'];?></h4>
		<table style="margin-top: 12px;"
			class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Image</th>
					<th>Name</th>
					<th>Author</th>
					<th>Publiser</th>
					<th>Created Date</th>
				</tr>
			</thead>
			<tbody>
			<?php while(($book = $books->fetch_assoc()) != null){?>
				<tr>
					<td><a href="book_info.php?book_id=<?php echo $book['id'];?>"><img
							src="<?php if(isset($book['cover_picture'])){echo "../".$book['cover_picture'];}?>"
							height=100px width=70px></a></td>
					<td><a href="book_info.php?book_id=<?php echo $book['id'];?>"><?php echo $book['name'];?></a></td>
					<td><?php echo $book['author'];?></td>
					<td><?php echo $book['published'];?></td>
					<td><?php echo $book['creation_ts'];?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>