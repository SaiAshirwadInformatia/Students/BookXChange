<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['admin'] ) or empty ( $_SESSION ['admin'] ) == true) {
	header ( "Location: inc_header" );
	exit();
}


if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
	
	$fields = array (
			"fname",
			"lname",
			"email",
			"password",
			"username",
			"gender" 
	);
	
	foreach ( $fields as $field ) {
		if (isset ( $_POST [$field] ) and ! empty ( $_POST [$field] ) == true) {
			if ($field == 'password') {
				$$field = md5 ( $_POST [$field] );
			} else {
				$$field = $_POST [$field];
			}
			$isValid = true;
		} else {
			$isValid = false;
			break;
		}
	}
	
	if ($isValid) {
		$query = "INSERT INTO users";
		$query .= "(fname, lname, email, password, username, gender)";
		$query .= "VALUES ('$fname', '$lname', '$email', '$password', '$username', '$gender')";	
		if ($mysqli->query ( $query )) {
			echo '<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<strong>User Created Successfully</strong>
				</div>';
		} else {
			echo '<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					Please Enter Other username / email id
				</div>';
		}
	} else {
		echo '<div class="alert alert-warning alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<strong>Please fill all the required field</strong>
			</div>';
	}
}


require_once 'inc_header.php';

require_once 'inc_nav.php';
?>

<div class="col-md-9">
	<form class="form-horizontal" action="" method="POST">
		<h2 align="center">Create User</h2>
		<div class="row">
			<div class="col-md-6">
				<label for="fname">Firstname </label> <input type="text"
					name="fname" class="form-control" placeholder="(ex: Akshay)" />
			</div>
			<div class="col-md-6">
				<label for="lname">Lastname</label> <input type="text"
					class="form-control" name="lname" placeholder="(ex: Mane)" />
			</div>
		</div>
		<div class="row" style="padding-top: 10px">
			<div class="col-md-6">
				<label for="email">Email ID</label> <input type="email"
					class="form-control" name="email" placeholder="johnsmith@gmail.com" />
			</div>
			<div class="col-md-6">
				<label for="password">Password</label> <input type="password"
					class="form-control" name="password" placeholder="(ex: **********)" />
			</div>
		</div>
		<div class="row" style="padding-top: 10px">
			<div class="col-md-6">
				<label for="username">Username</label> <input type="text"
					class="form-control" name="username" placeholder="(ex: akshaym)" />
			</div>
			<div class="col-md-3">
				<label for="gender"> Gender</label>
				<div class="radio">
					<label> <input type="radio" name="gender" value="m" />
						Male
					</label> <label> <input type="radio" name="gender" value="f" />
						Female
					</label>
				</div>
			</div>
		</div>
		<div class="row" style="padding-top: 10px">
			<div class="col-md-2 col-md-offset-5">
				<button type="submit" class="btn btn-primary ">Register</button>
			</div>
		</div>

	</form>
</div>