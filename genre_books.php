<?php
require_once 'db_connect.php';
if (isset ( $_GET ['genre_id'] )) {
	$genre_id = $_GET ['genre_id'];
	
	$query = "SELECT * FROM books WHERE genre_id = $genre_id AND is_approved = 1 AND id NOT IN(SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap)";
	$books = $mysqli->query ( $query );
	$book = $books->fetch_assoc ();
	
	$query = "SELECT * FROM genres WHERE id = $genre_id";
	$genre = $mysqli->query ( $query );
	$genre = $genre->fetch_assoc ();
} else {
	header ( "Location: books.php" );
	exit ();
}

require_once 'inc_header.php';

$page = "genre_books.php";
require_once 'hits.php';

require_once 'inc_nav.php';
?>

<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="books.php"><span class="glyphicon glyphicon-book"></span>
				Books</a></li>
		<li class="active"><span class="glyphicon glyphicon-th-list"></span>
				<?php echo $genre['name'];?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<h4><?php echo $genre['name'];?></h4>
			<hr>
<?php
if ($books->num_rows > 0) {
	while ( ($book = $books->fetch_assoc ()) != null ) {
		require 'inc_book.php';
	}
} else {
	?>

					<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<strong>Oops, looks like no books are added in <code><?php echo $genre['name']?></code>
					genre
				</strong>
			</div>
<?php
}
?>
                </div>
	</div>
</div>

<?php
require_once 'inc_footer.php';
?>
