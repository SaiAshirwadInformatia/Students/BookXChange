<?php
require_once 'db_connect.php';

if (isset ( $_GET ['user_id'] ) and ! empty ( $_GET ['user_id'] ) == true) {
	$user_id = $_GET ['user_id'];
	$book_id = $_GET ['book_id'];
	$swapped_user_id = $_GET ['swapped_user_id'];
	$swapped_book_id = $_GET ['swapped_book_id'];
	$phone = $_GET ['phone'];
	$access_token = md5 ( $user_id ) . '' . md5 ( time () );
	$query = "INSERT INTO swap (`user_id`, `book_id`, `swapped_user_id`, `swapped_book_id`, `access_token`, user_phone) VALUES";
	$query .= "('$user_id', '$book_id', '$swapped_user_id', '$swapped_book_id' , '$access_token', '$phone')";
	if ($mysqli->query ( $query )) {
		$query = "SELECT * FROM users WHERE id = $user_id";
		$user_name = $mysqli->query ( $query );
		$user_name = $user_name->fetch_assoc ();
		
		$query = "SELECT * FROM users WHERE id = $swapped_user_id";
		$swapped_user_name = $mysqli->query ( $query );
		$swapped_user_name = $swapped_user_name->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = $book_id";
		$user_book = $mysqli->query ( $query );
		$user_book = $user_book->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = $swapped_book_id";
		$swapped_user_book = $mysqli->query ( $query );
		$swapped_user_book = $swapped_user_book->fetch_assoc ();
		
		$query = "SELECt * FROM swap WHERE access_token = '$access_token'";
		$swap = $mysqli->query ( $query );
		$swap = $swap->fetch_assoc ();
		
		$mail->addAddress ( $swapped_user_name ['email'], $swapped_user_name ['fname'] . ' ' . $swapped_user_name ['lname'] );
		$message = file_get_contents ( 'templates/swapped_book_user.txt' );
		$message = str_replace ( "*|USER_NAME|*", $user_name ['fname'], $message );
		$message = str_replace ( "*|USER_BOOK|*", $user_book ['name'], $message );
		$message = str_replace ( "*|SWAPPED_USER_NAME|*", $swapped_user_name ['fname'], $message );
		$message = str_replace ( "*|SWAPPED_USER_BOOK|*", $swapped_user_book ['name'], $message );
		$message = str_replace ( "*|ACCESS_TOKEN|*", $swapped_user_name ['access_token'], $message );
		$message = str_replace ( "*|SWAP_AT|*", $swap ['access_token'], $message );
		$mail->Subject = "Book Swapping Notification - BookXchange";
		$message = nl2br ( $message );
		$mail->msgHTML ( $message );
		
		if ($mail->send ()) {
		} else {
			echo $mail->ErrorInfo;
		}
		
		$mail->clearAddresses ();
		$mail->addAddress ( $user_name ['email'], $user_name ['fname'] . ' ' . $user_name ['lname'] );
		$message = file_get_contents ( 'templates/book_user.txt' );
		$message = str_replace ( "*|USER_NAME|*", $user_name ['fname'], $message );
		$message = str_replace ( "*|USER_BOOK|*", $user_book ['name'], $message );
		$message = str_replace ( "*|SWAPPED_USER_BOOK|*", $swapped_user_book ['name'], $message );
		$message = nl2br ( $message );
		$mail->msgHTML ( $message );
		$mail->Subject = "Book Swapping Notification - BookXchange";
		if ($mail->send ()) {
		} else {
			echo $mail->ErrorInfo;
		}
	}
	$_SESSION ['mesaage'] = true;
}
if (isset ( $_SESSION ['logged'] ) and ! empty ( $_SESSION ['logged'] ) == true) {
	$user_id = $_SESSION ['loggeduser'] ['id'];
	$query = "SELECT * FROM swap WHERE user_id = '" . $user_id . "' and is_approved = 0";
	$swaps = $mysqli->query ( $query );
} else {
	header ( "Location: index.php" );
	exit ();
}

require_once 'inc_header.php';

if (isset ( $_SESSION ['shared'] ) and ! empty ( $_SESSION ['shared'] ) == true) {
	?>
<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<p>Your contact successfully shared.</p>
</div>
<?php
	unset ( $_SESSION ['shared'] );
}
require_once 'inc_nav.php';
?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="swapped_book.php"><span class="mdi-action-swap-vert"></span>
				Swapped Books</a></li>
		<li><a href="swap_request.php"><span
				class="glyphicon glyphicon-th-list"></span> Swap Books Request</a></li>
		<li class="active"><span class="glyphicon glyphicon-dashboard"></span>
			Waiting Swap Books</li>
	</ol>
<?php

if ($_SESSION ['mesaage'] and ! empty ( $_SESSION ['mesaage'] ) == true) {
	unset ( $_SESSION ['mesaage'] );
	?>
		<div class="jumbotron">
		<h4>
			Thank You for swapping this book, we have notified the book
			<code>owner</code>
			for swapping of book.
		</h4>
		<h4>
			If the owner of book, approves you will be notified with his
			<code>contact details.</code>
		</h4>
	</div>
	<?php }?>
	<h4>Waiting Your Book For Swap</h4>
<?php
$i = 0;
if (is_object ( $swaps ) and $swaps->num_rows > 0) {
	while ( ($swap = $swaps->fetch_assoc ()) != null ) {
		$i = $i + 1;
		$swapped_user_id = $swap ['swapped_user_id'];
		$swapped_book_id = $swap ['swapped_book_id'];
		
		$user_id = $swap ['user_id'];
		$book_id = $swap ['book_id'];
		?>
<div class="row">
		<div style="padding-bottom: 20px; padding-top: 20px;">
			<div class="col-md-5 panel panel-primary">
	<?php
		// User and User's Book information
		$query = "SELECT * FROM users WHERE id = " . $user_id;
		$user = $mysqli->query ( $query );
		$user = $user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $book_id;
		$book = $mysqli->query ( $query );
		$book = $book->fetch_assoc ();
		
		// Swap User and swap User's Book information
		$query = "SELECT * FROM users WHERE id = " . $swapped_user_id;
		$swapped_user = $mysqli->query ( $query );
		$swapped_user = $swapped_user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $swapped_book_id;
		$swapped_book = $mysqli->query ( $query );
		$swapped_book = $swapped_book->fetch_assoc ();
		?><div class="panel-heading">
					<h4 class="panel-title"><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $user ['fname'] . "'s Book";
		}
		?></h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<img alt="<?php echo $book['name']?>"
							src="<?php echo $book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if (($user ['id']) == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-2" align="center">
				<h2><?php echo $i;?></h2>
				<h4 align="center">Swapped Deatils</h4>
				<table class="table table-condensed">
					<hr>
					<tr class="active">
						<td><b>Requested By</b></td>
						<td><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
					</tr>
					<tr class="active">
						<td><b>Swapping Date</b></td>
						<td><?php echo $swap['creation_ts'];?></td>
					</tr>
				</table>
				<?php if($swapped_user['id'] == $_SESSION['loggeduser']['id']){?>
				<button class="mdi-av-repeat btn btn-success btn-sm">
					<small>Swap</small>
				</button>
				<?php }?>
			</div>
			<div class="col-md-5 panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
			<?php
		
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $swapped_user ['fname'] . "'s Book";
		}
		?>
			</h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $swapped_book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $swapped_user ['fname'] . ' ' . $swapped_user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $swapped_book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $swapped_book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $swapped_book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $swapped_book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $swapped_book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $swapped_book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $swapped_book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-6" align="center">
						<img alt="<?php echo $swapped_book['name']?>"
							src="<?php echo $swapped_book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	}
} else {
	?>
<div class="alert alert-dismissable alert-warning">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<p>There are no waiting swap books.</p>
	</div>
<?php
}
?>
</div>
<?php

require_once 'inc_footer.php';
?>
