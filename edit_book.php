<?php
require_once 'db_connect.php';
$query = "SELECT * FROM genres";
$genres = $mysqli->query ( $query );
if (! isset ( $_SESSION ['logged'] ) or $_SESSION ['logged'] == false) {
	header ( "Location: index.php" );
	exit ();
}
$page = "edit_book.php";
require_once 'hits.php';

if (isset ( $_SESSION ['loggeduser'] ['id'] )) {
	$user_id = $_SESSION ['loggeduser'] ['id'];
}
$book = array (
		"name" => '',
		"description" => '',
		"author" => '',
		"published" => '',
		"published_year" => '',
		"pages" => '',
		"isbn10" => '',
		"isbn13" => '',
		"language" => '',
		"genre_id" => '',
		"price" => '',
		"cover_picture" => '' 
);
if (isset ( $_GET ['book_id'] )) {
	$query = "SELECT * FROM books WHERE id = " . $_GET ['book_id'];
	$book_r = $mysqli->query ( $query );
	if (isset ( $book_r ) and $book_r->num_rows > 0) {
		$book = $book_r->fetch_assoc ();
	}
}

if (isset ( $_POST ) and count ( $_POST ) > 0) {
	$isValid = true;
	
	$_POST ['lastmodified_ts'] = date ( "Y-m-d h:m:s", time () );
	foreach ( $book as $field => $value ) {
		// var_dump ( $field, (! isset ( $_POST [$field] ) or empty ( $_POST [$field] )) );
		if ($field == 'is_approved') {
			continue;
		}
		if ((! isset ( $_POST [$field] ) or empty ( $_POST [$field] ))) {
			$isValid = ! $isValid;
			break;
		} else {
			if ($field == "cover_picture") {
				if (isset ( $_FILES ['cover_picture'] ) and ! empty ( $_FILES ['cover_picture'] ) == true and $_FILES ['cover_picture'] ['error'] == 0) {
					$_POST ['cover_picture'] = $move = "upload/book/" . md5 ( $_FILES ['cover_picture'] ['name'], $_SERVER ['loggeduser'] ['id'] );
					move_uploaded_file ( $_FILES ['cover_picture'] ['tmp_name'], $move );
					if (isset ( $_GET ['book_id'] )) {
						$query1 = "SELECT * FROM books WHERE id =" . $_GET ['book_id'];
						$rm_picture = $mysqli->query ( $query1 );
						$rm_picture = $rm_picture->fetch_assoc ();
						if (isset ( $rm_picture ['cover_picture'] ) and file_exists ( $rm_picture ['cover_picture'] )) {
							if (! unlink ( $rm_picture ['cover_picture'] )) {
								echo "Book image Deletion failed";
							}
						}
					}
					$$field = $_POST ['cover_picture'];
				} else {
					if (! isset ( $rm_picture ['cover_picture'] ) and empty ( $_POST ['cover_picture'] )) {
						$isValid = false;
					}
				}
			} elseif ($isValid and (is_string ( $_POST [$field] ))) {
				$$field = $mysqli->escape_string ( $_POST [$field] );
			} else if ($isValid) {
				$$field = $_POST [$field];
			} else {
				$isValid != $isValid;
				break;
			}
		}
	}
	
	if ($isValid) {
		
		if (isset ( $_GET ['book_id'] )) {
			$query = "UPDATE books ";
			$set_array = array ();
			$book = array_merge ( $book, $_POST );
			foreach ( $book as $field => $value ) {
				if ($field == 'id' or $field == 'creation_ts') {
					continue;
				}
				if ($field == 'cover_picture') {
					if (isset ( $_FILES ['cover_picture'] ) and ! empty ( $_FILES ['cover_picture'] ) == true and $_FILES ['cover_picture'] ['error'] == 0) {
						$move = "upload/book/" . md5 ( $_FILES ['cover_picture'] ['name'] );
						move_uploaded_file ( $_FILES ['cover_picture'] ['tmp_name'], $move );
						$_POST ['cover_picture'] = $move;
						if (isset ( $_GET ['book_id'] )) {
							$query1 = "SELECT * FROM books WHERE id =" . $_GET ['book_id'];
							$rm_picture = $mysqli->query ( $query1 );
							$rm_picture = $rm_picture->fetch_assoc ();
							if (isset ( $rm_picture ['cover_picture'] ) and file_exists ( $rm_picture ['cover_picture'] )) {
								if (! unlink ( $rm_picture ['cover_picture'] )) {
									echo "Book image Deletion failed";
								}
							}
						}
						$set_array [] = " $field = '" . $_POST ['cover_picture'] . "'";
					}
				} elseif (is_string ( $value )) {
					$set_array [] = " $field = '" . $mysqli->escape_string ( $value ) . "'";
				} else {
					$set_array [] = " $field = $value ";
				}
			}
			$query .= " SET " . implode ( ", ", $set_array ) . " WHERE id = " . $_GET ['book_id'];
		} else {
			$query = "INSERT INTO books (`name`, `description`, `author`, `published`, `published_year`, `pages`, `isbn10`,
             `isbn13`, `language`, `genre_id`, `user_id`, `price`, `cover_picture`) ";
			$query .= "VALUES ('$name', '$description', '$author', '$published', '$published_year', '$pages',
         '$isbn10', '$isbn13', '$language', '$genre_id', '$user_id', '$price', '$move')";
		}
		if ($mysqli->query ( $query )) {
			if (! isset ( $_GET ['book_id'] )) {
				$query = "SELECT * FROM users WHERE id = $user_id";
				$user = $mysqli->query ( $query );
				$user = $user->fetch_assoc ();
				$mail->addAddress ( $user ['email'], $user ['fname'] . ' ' . $user ['lname'] );
				$message = file_get_contents ( 'templates/add_book.txt' );
				$message = str_replace ( "*|FNAME|*", $user ['fname'], $message );
				$message = str_replace ( "*|BOOK_NAME|*", $name, $message );
				$message = nl2br ( $message );
				$mail->msgHTML ( $message );
				$mail->Subject = "Book Added Successfully - BookXchange";
				if ($mail->send ()) {
				} else {
					echo $mail->ErrorInfo;
				}
			}
			?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<?php
			
			if (isset ( $_GET ['book_id'] )) {
				echo '<strong>Book updated successfully';
			} else {
				echo '<strong>Book added successfully.</strong>';
			}
			?>
</div>
<?php
		} else {
			?>
<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Failed: <?php echo "Please Not use Apostrophe".$query,$mysqli->error;die();?></strong>
</div>
<?php
		}
	} else {
		?>
<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Please fill all the required fields</strong>
</div>
<?php
	}
}

?>

    <?php
				
				require_once 'inc_header.php';
				require_once 'inc_nav.php';
				?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="books.php"><span class="glyphicon glyphicon-book"></span>
				Books</a></li>
		<li class="active"><span class="glyphicon glyphicon-plus"></span> Add
			Book</li>
	</ol>
	<form class="form-horizontal" enctype="multipart/form-data" action=""
		method="POST">
		<fieldset>
			<h4>
				Enter Information About Book <?php if($_GET){if($book['user_id'] == $_SESSION['loggeduser']['id']){?><a
					class="btn btn-danger pull-right"
					href="delete_book.php?book_id=<?php echo $book['id'];?>"
					style="margin-top: -3px"><span class="mdi-action-delete"></span></a>
					<?php }}?>
			</h4>
			<hr>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Name</label>
				<div class="col-lg-9">
				<?php
				
				if (isset ( $book ['id'] )) {
					echo '<input type="hidden" name="id" value="' . $book ['id'] . '" />';
				}
				if (isset ( $book ['is_active'] )) {
					echo '<input type="hidden" name="is_active" value="' . $book ['is_active'] . '" />';
				}
				if (isset ( $book ['cover_picture'] ) and ! empty ( $book ['cover_picture'] )) {
					echo '<input type="hidden" name="cover_picture" value="' . $book ['cover_picture'] . '" />';
				} else {
					echo '<input type="hidden" name="cover_picture" value="images/book_image.jpg" />';
				}
				?>  <input type="hidden" name="user_id"
						value="<?php echo $user_id;?>" /> <input type="text"
						class=" form-control" name="name" id="inputDefault"
						placeholder="(ex: Lord of Rings)"
						value="<?php echo $book['name'];?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="inputDefault">Book Author</label>
				<div class="col-lg-9">
					<input type="text" class=" form-control" name="author"
						id="inputDefault" placeholder="(ex: J. R. R. Tolkien)"
						value="<?php echo $book['author'];?>">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publisher</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="published"
						placeholder="(ex: George Allen & Unwin "
						value="<?php echo $book['published'];?>">
				</div>
				<label for="textArea" class="col-md-2 control-label">Price</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="price"
						placeholder="(ex: 230)" value="<?php echo $book['price'];?>">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Publication
					Date</label>
				<div class="col-md-3">
					<input type="date" class="form-control" name="published_year"
						placeholder="(ex: YYYY-MM-DD"
						value="<?php echo $book['published_year'];?>">
				</div>
				<label for="textArea" class="col-md-2 control-label">Language</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="language"
						placeholder="(ex: English)"
						value="<?php echo $book['language'];?>">
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">Genre</label>
				<div class="col-md-3">
					<select class="form-control" id="select" name="genre_id">
						<option>--Select--</option>
				    	   	<?php
													while ( ($genre = $genres->fetch_assoc ()) != null ) {
														echo '<option value="' . $genre ['id'] . '"' . (($genre ['id'] == $book ['genre_id']) ? 'selected' : '') . '>' . $genre ['name'] . '</option>';
													}
													?>
					</select>
				</div>
				<label for="inputDefault" class="col-md-2 control-label">Pages</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="pages"
						id="inputEmail" placeholder="(ex: 350)"
						value="<?php echo $book['pages'];?>">
				</div>
			</div>
			<div class="form-group">
				<label for="inputDefault" class="col-md-2 control-label">ISBN-13 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn13"
						id="inputPassword" placeholder="(ex: 9780689869914)"
						value="<?php echo $book['isbn13'];?>">
				</div>
				<label for="inputDefault" class="col-md-2 control-label">ISBN-10 No</label>
				<div class="col-md-3">
					<input type="text" class="form-control" name="isbn10"
						id="inputPassword" placeholder="(ex: 9780689869)"
						value="<?php echo $book['isbn10'];?>">
				</div>
			</div>
			<div class="form-group">
				<label for="textArea" class="col-md-2 control-label">Description</label>
				<div class="col-md-9">
					<textarea class="form-control" id="textArea"
						placeholder="Any information about Book condition / features"
						name="description"><?php echo $book['description'];?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Book Image</label>
				<div class="col-md-9">
					<input type="file" name="cover_picture" class="form-control" />
				</div>
			</div>
			<?php
			
			if (isset ( $book ['creation_ts'] )) {
				echo '<div class="form-group"><label class="col-md-2 control-label">Created At</label><p class="col-md-9 form-control-static">' . $book ['creation_ts'] . '</p>
    <input type="hidden" name="creation_ts" value="' . $book ['creation_ts'] . '" /></div>';
			}
			
			if (isset ( $book ['lastmodified_ts'] )) {
				echo '<div class="form-group"><label class="col-md-2 control-label">Last Modified At</label><p class="col-md-9 form-control-static">' . $book ['lastmodified_ts'] . '</p>
    <input type="hidden" name="lastmodified_ts" value="' . $book ['lastmodified_ts'] . '" /></div>';
			}
			?>
			<div class="form-group">
				<div class="col-lg-9 col-lg-offset-2">
					<button type="submit" class="btn btn-primary"><?php
					
					if (isset ( $_GET ['book_id'] ) and ! empty ( $_GET ['book_id'] ) == true) {
						echo "Update Book";
					} else {
						echo "Add Book";
					}
					?></button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script>
$(document).ready(function(){
    tinymce
	.init({
	    selector : "textarea",
	    relative_urls : true,
	    remove_script_host : false
	});
});
</script>
<?php
require_once 'inc_footer.php';
?>
