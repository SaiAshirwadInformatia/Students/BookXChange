<?php
require_once 'db_connect.php';
if (isset ( $_GET ['at'] ) and isset ( $_GET ['swap_at'] ) and (! empty ( $_GET ['at'] ) and ! empty ( $_GET ['swap_at'] )) == true) {
	$query = "SELECT * FROM users WHERE access_token ='" . $_GET ['at'] . "'";
	$user = $mysqli->query ( $query );
	$query = "SELECT * FROM swap
WHERE access_token ='" . $_GET ['swap_at'] . "' AND is_approved = 0";
	$swaps = $mysqli->query ( $query );
	if (is_object ( $swaps ) and $swaps->num_rows > 0) {
		if (is_object ( $user ) and $user->num_rows > 0) {
			$_SESSION ['loggeduser'] = $user->fetch_assoc ();
			$_SESSION ['logged'] = true;
			$_SESSION ['login_failed'] = false;
		} else {
			header ( "Location:
index.php" );
			exit ();
		}
	} else {
		header ( "Location: index.php" );
		exit ();
	}
} else {
	if (isset ( $_SESSION ['logged'] ) and ! empty ( $_SESSION ['logged'] ) == true) {
		$user_id = $_SESSION ['loggeduser'] ['id'];
		$query = "SELECT * FROM swap WHERE swapped_user_id = '" . $user_id . "' and is_approved = 0";
		$swaps = $mysqli->query ( $query );
	} else {
		header ( "Location: index.php" );
		exit ();
	}
}
require_once 'inc_header.php';
require_once 'inc_nav.php';
?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="swapped_book.php"><span class="mdi-action-swap-vert"></span>
				Swapped Books</a></li>
		<li class="active"><span class="glyphicon glyphicon-th-list"></span>
			Swap Books Request</li>
		<li><a href="waiting_swap.php"><span
				class="glyphicon glyphicon-dashboard"></span> Waiting Swap Books</a></li>
	</ol>
	<h4>Swap Request</h4>
<?php
$i = 0;
if (is_object ( $swaps ) and $swaps->num_rows > 0) {
	while ( ($swap = $swaps->fetch_assoc ()) != null ) {
		$i = $i + 1;
		$swapped_user_id = $swap ['swapped_user_id'];
		$swapped_book_id = $swap ['swapped_book_id'];
		
		$user_id = $swap ['user_id'];
		$book_id = $swap ['book_id'];
		?>
			<div class="row">
		<div style="padding-bottom: 20px; padding-top: 20px;">
			<div class="col-md-5 panel panel-primary">
	<?php
		// User and User's Book information
		$query = "SELECT * FROM users WHERE id = " . $user_id;
		$user = $mysqli->query ( $query );
		$user = $user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $book_id;
		$book = $mysqli->query ( $query );
		$book = $book->fetch_assoc ();
		
		// Swap User and swap User's Book information
		$query = "SELECT * FROM users WHERE id = " . $swapped_user_id;
		$swapped_user = $mysqli->query ( $query );
		$swapped_user = $swapped_user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $swapped_book_id;
		$swapped_book = $mysqli->query ( $query );
		$swapped_book = $swapped_book->fetch_assoc ();
		?><div class="panel-heading">
					<h4 class="panel-title"><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $user ['fname'] . "'s Book";
		}
		?></h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<img alt="<?php echo $book['name']?>"
							src="<?php echo $book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if (($user ['id']) == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-2" align="center">
				<h2><?php echo $i;?></h2>
				<h4 align="center">Swapped Deatils</h4>
				<hr>
				<table class="table table-condensed">
					<tr class="active">
						<td><b>Requested By</b></td>
						<td><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
					</tr>
					<tr class="active">
						<td><b>Swapping Date</b></td>
						<td><?php echo $swap['creation_ts'];?></td>
					</tr>
				</table>
				<?php if($swapped_user['id'] == $_SESSION['loggeduser']['id']){?>
				<a class="mdi-av-repeat btn btn-success btn-sm"
					href="contact_sharing.php?swap_at=<?php echo $swap['access_token'];?>"><small>Swap</small>
				</a>
				<?php }?>
			</div>
			<div class="col-md-5 panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
			<?php
		
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $swapped_user ['fname'] . "'s Book";
		}
		?>
			</h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $swapped_book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $swapped_user ['fname'] . ' ' . $swapped_user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $swapped_book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $swapped_book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $swapped_book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $swapped_book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $swapped_book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $swapped_book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $swapped_book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-6" align="center">
						<img alt="<?php echo $swapped_book['name']?>"
							src="<?php echo $swapped_book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	}
} else {
	?>
	<div class="alert alert-dismissable alert-warning">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<p>There are no swap request.</p>
	</div>
	<?php
}
?>
	
</div>
<?php
require_once 'inc_footer.php';
?>