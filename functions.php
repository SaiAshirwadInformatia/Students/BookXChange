<?php

function truncate($str, $length = 100, $append = "&hellip;")
{
    $str = trim($str);
    
    if (strlen($str) > $length) {
        $str = substr($str, 0, $length);
        $str = explode("\n", $str);
        $str = array_shift($str) . $append;
    }
    
    return $str;
}