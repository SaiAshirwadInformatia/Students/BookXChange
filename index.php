<?php
require_once 'db_connect.php';

$query = "SELECT * FROM books WHERE id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND is_approved = 1 ORDER BY rand() LIMIT 0, 4";
$books = $mysqli->query ( $query );
require_once 'inc_header.php';
require_once 'hits.php';

if (isset ( $_SESSION ['login_failed'] ) and ! empty ( $_SESSION ['login_failed'] ) == true) {
	echo '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x	</button>
	<strong>Please check your email or password !</strong>
</div>';
	unset ( $_SESSION ['login_failed'] );
} else if (isset ( $_SESSION ['please_login'] ) and ! empty ( $_SESSION ['please_login'] ) == true) {
	echo '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Please login !</strong>
</div>';
	unset ( $_SESSION ['please_login'] );
} else if (isset ( $_SESSION ['validation_failed'] ) and ! empty ( $_SESSION ['validation_failed'] ) == true) {
	$row = $_SESSION['validation_failed_info'];
	$fname = $row['fname'];
	$lname = $row['lname'];
	$email =$row['email'];
	$username = $row['username'];
	$access_token = $row['access_token'];
	$mail->addAddress ( $email, $fname . ' ' . $lname );
	$message = file_get_contents ( 'templates/user_validation.txt' );
	$message = str_replace ( "*|FNAME|*", $fname, $message );
	$message = str_replace ( "*|USERNAME|*", $username, $message );
	$message = str_replace ( "*|ACCESS_TOKEN|*", $access_token, $message );
	$message = nl2br ( $message );
	$mail->msgHTML ( $message );
	$mail->Subject = "Confirm your Account - BookXchange";
	if (!$mail->send ()) {
		echo "$mail->ErrorInfo";
	}
	echo '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Check your email for validation of your account.</strong>
</div>';
	unset ( $_SESSION ['validation_failed'] );
}

if (isset ( $_GET ['at'] )) {
	$access_token = $_GET ['at'];
	$query = "SELECT * FROM users WHERE access_token = '$access_token'";
	$valid_user = $mysqli->query ( $query );
	if ($valid_user and $valid_user->num_rows > 0) {
		$valid_user = $valid_user->fetch_assoc ();
		$query = "UPDATE users SET is_active = 1 WHERE access_token = '$access_token'";
		if ($mysqli->query ( $query )) {
			echo '<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Hi ' . ucfirst ( $valid_user ['fname'] ) . '</strong>, Now you are able to login.</div>';
		} else {
			echo '<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Hi ' . ucfirst ( $valid_user ['fname'] ) . '</strong>, you are not able to login because you are not complete the validation</div>';
		}
	} else {
		echo '<div class="alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>You are not BookXchage user, You want register on BookXchange <a herf="register.php" class="alert-link">click here</a>	</strong></div>';
	}
}

require_once 'inc_nav.php';

?>
<div class="col-md-11 col-xs-11">
	<div class="row">    
            <?php require_once 'inc_carousal.php';?>
    </div>
	<div class="row">
				<?php
				if ($books and $books->num_rows > 0) {
					while ( ($book = $books->fetch_assoc ()) != null ) {
						
						require 'inc_book.php';
					}
				}
				?>	
	</div>
</div>
<?php
require_once 'inc_footer.php';
?>
