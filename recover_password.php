<?php
require_once 'db_connect.php';

if (! isset ( $_GET ['at'] )) {
	if (isset ( $_POST ['recover_password'] )) {
		$email = $_POST ['email'];
		$query = "SELECT * FROM users WHERE email = '" . $email . "'";
		$user = $mysqli->query ( $query );
		if (is_object ( $user ) and $user->num_rows > 0) {
			$user = $user->fetch_assoc ();
			$mail->addAddress ( $user ['email'], $user ['fname'] . ' ' . $user ['lname'] );
			$message = file_get_contents ( 'templates/recover_password.txt' );
			$message = str_replace ( "*|FNAME|*", $user ['fname'], $message );
			$message = str_replace ( "*|ACCESS_TOKEN|*", $user ['access_token'], $message );
			$message = nl2br ( $message );
			$mail->msgHTML ( $message );
			$mail->Subject = "Recover Password - BookXchange";
			if ($mail->send ()) {
				$_SESSION ['correct_mail'] = true;
			} else {
				echo $mail->ErrorInfo;
			}
		} else {
			$_SESSION ['invalid_mail'] = true;
		}
	}
	
	require_once 'inc_header.php';
	
	if (isset ( $_SESSION ['correct_mail'] ) and ! empty ( $_SESSION ['correct_mail'] ) == true) {
		echo '<div class="alert alert-dismissable alert-success">
					    <button type="button" class="close" data-dismiss="alert">x</button>
					    Check your email account for instructions on how to <strong> reset your password</strong>.</div>';
		unset ( $_SESSION ['correct_mail'] );
	} else if (isset ( $_SESSION ['invalid_mail'] ) and ! empty ( $_SESSION ['invalid_mail'] ) == true) {
		echo '<div class="alert alert-dismissable alert-warning">
			    <button type="button" class="close" data-dismiss="alert">x</button>
			    <p></a>Please enter correct Email Id</p></div>';
		unset ( $_SESSION ['invalid_mail'] );
	}
	
	require_once 'inc_nav.php';
	?>
<div class="jumbotron col-md-11">
	<p>Enter the email address you use to log in to your account.</p>
</div>
<hr>
<div class="col-md-4">
	<form action="" method="POST">
		<div class="form-group">
			<label class="form-control">Enter your Email ID</label> <input
				type="email" class="form-control" name="email"
				placeholder="(ex. abc@gmail.com)" />
		</div>
		<div class="form-group">
			<button class="btn btn-success btn-sm" type="submit"
				name="recover_password">Recover Password</button>
		</div>
	</form>
</div>

<?php
} else {
	$access_token = $_GET ['at'];
	$query = "SELECT * FROM users WHERE access_token = '" . $access_token . "'";
	$user = $mysqli->query ( $query );
	if (is_object ( $user ) and $user->num_rows > 0) {
		if (isset ( $_POST ['change_password'] )) {
			if ($_POST ['new_password'] == $_POST ['confirm_password']) {
				if (strlen ( $_POST ['confirm_password'] ) >= 6) {
					$query = "UPDATE users SET password = '" . md5 ( $_POST ['confirm_password'] ) . "' WHERE access_token = '" . $access_token . "'";
					if ($mysqli->query ( $query )) {
						$_SESSION ['successfully_changed'] = true;
					} else {
						$_SESSION ['change_failed'] = true;
					}
				} else {
					$_SESSION ['password_length'] = true;
				}
			} else {
				$_SESSION ['correct_password'] = true;
			}
		}
	} else {
		header ( "Location: index.php" );
		exit ();
	}
	require_once 'inc_header.php';
	
	if (isset ( $_SESSION ['successfully_changed'] ) and ! empty ( $_SESSION ['successfully_changed'] ) == true) {
		echo '<div class="alert alert-dismissable alert-success">
			<button type="button" class="close" data-dismiss="alert">x</button>
			 <strong>Well done!</strong> Your password has been changed successfully.</div>';
		unset ( $_SESSION ['successfully_changed'] );
	} else if (isset ( $_SESSION ['change_failed'] ) and ! empty ( $_SESSION ['change_failed'] ) == true) {
		echo '<div class="alert alert-dismissable alert-danger">
			    <button type="button" class="close" data-dismiss="alert">x</button>Password failed to change</div>';
		unset ( $_SESSION ['change_failed'] );
	} else if (isset ( $_SESSION ['password_length'] ) and ! empty ( $_SESSION ['password_length'] ) == true) {
		echo '<div class="alert alert-dismissable alert-warning">
    <button type="button" class="close" data-dismiss="alert">x</button>Password must be 6 or more characters</div>';
		unset ( $_SESSION ['password_length'] );
	} else if (isset ( $_SESSION ['correct_password'] ) and ! empty ( $_SESSION ['correct_password'] ) == true) {
		echo '<div class="alert alert-dismissable alert-warning">
    <button type="button" class="close" data-dismiss="alert">x</button>Please enter correct password.</div>';
		unset ( $_SESSION ['correct_password'] );
	}
	require_once 'inc_nav.php';
	?>

<div class="col-md-11">
	<div class="col-md-4 col-md-offset-4 jumbotron">
		<form action="" method="POST">
			<div class="form-group">
				<label class="form-control">New Password</label> <input
					class="form-control" type="password" name="new_password"
					data-hint="Password should be minimun 6 charcters" />
			</div>
			<div class="form-group">
				<label class="form-control">Confirm Password</label> <input
					class="form-control" type="password" name="confirm_password" />
			</div>
			<center>
				<button class="btn btn-primary btn-sm" type="submit"
					name="change_password">Change Password</button>
			</center>
		</form>
	</div>
</div>
<?php
}
?>