<?php

require_once 'db_connect.php';


$query = "SELECT * FROM books WHERE id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND is_approved = 1 ORDER BY id desc LIMIT 0, 4";

$books = $mysqli->query ( $query );


require_once 'inc_header.php';

$page = "book.php";
require_once 'hits.php';

require_once 'inc_nav.php';

?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li class="active"><span class="glyphicon glyphicon-book"></span>
			Books</li>
		<li><a href="edit_book.php"><span class="glyphicon glyphicon-plus"></span>
				Add Book</a></li>
	</ol>
	<h4>Recently Added</h4>
	<hr>
	<div class="row">
		<div class="col-md-12">
		<?php
		if ($books and $books->num_rows > 0) {
			while ( ($book = $books->fetch_assoc ()) != null ) {
				require 'inc_book.php';
			}
		} else {
			echo '
        <div class="alert alert-danger" role="alert">
        <p>Opps, looks like no books are recently added by System Admin Yet</p>
        </div>';
		}
		?>
	</div>

		<h4>Books By Genre</h4>
		<hr>
		<div class="col-md-12">
		<?php
		
		$query = "SELECT G.*, COUNT(B.genre_id) AS `books` FROM genres G JOIN books B ON B.genre_id = G.id WHERE B.is_approved = 1 GROUP BY B.genre_id";
		$genres = $mysqli->query ( $query );
		if ($genres and $genres->num_rows > 0) {
			while ( ($genre = $genres->fetch_assoc ()) != null ) {
				?>
            <div class="col-md-3">
				<div class="thumbnail" style="margin-top: 10px">
					<h4 style="border-bottom: 1px solid">
						<a href="genre_books.php?genre_id=<?php echo $genre['id'];?>">
							<?php echo $genre['name'];?></a>
					</h4>
					<?php
				
				if (file_exists ( 'images/genre_images/' . ucfirst ( $genre ['name'] ) . '.jpg' )) {
					echo '<img src="images/genre_images/' . ucfirst ( $genre ['name'] ) . '.jpg" alt="' . ucfirst ( $genre ['name'] ) . '" title="' . ucfirst ( $genre ['name'] ) . '"
						style="height: 200px; width: 249px">';
				} else if (file_exists ( 'images/genre_images/' . ucfirst ( $genre ['name'] ) . '.png' )) {
					echo '<img src="images/genre_images/' . ucfirst ( $genre ['name'] ) . '.png" alt="' . ucfirst ( $genre ['name'] ) . '" title="' . ucfirst ( $genre ['name'] ) . '"
						style="height: 200px; width: 249px">';
				} else if (file_exists ( 'images/genre_images/' . ucfirst ( $genre ['name'] ) . '.gif' )) {
					echo '<img src="images/genre_images/' . ucfirst ( $genre ['name'] ) . '.gif" alt="' . ucfirst ( $genre ['name'] ) . '" title="' . ucfirst ( $genre ['name'] ) . '"
						style="height: 200px; width: 249px">';
				} else {
					?>
					<img src="images/book_image1.jpg" alt="..."
						style="height: 200px; width: 249px">
						<?php }?>
					<h4><?php
				echo "Total Books : " . $genre ['books'];
				?></h4>
				</div>
			</div>
			<?php
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><p>Oops, looks like no genres are added by System Admin yet</p></div>';
		}
		?>
		</div>
	</div>
</div>
<?php
require_once 'inc_footer.php';
?>
