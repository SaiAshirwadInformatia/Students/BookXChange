<div id="carousel-example-generic" class="carousel slide carousel-index"
	data-ride="carousel">

	<ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0"
			class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="item active">
			<img src="images/carousel1.jpg" alt="..." class="carousel-img">
		</div>
		<div class="item">
			<img src="images/carousel2.jpg" alt="..." class="carousel-img">
		</div>
		<div class="item">
			<img src="images/carousel3.jpg" alt="..." class="carousel-img" style="height: 252px">
		</div>
	</div>
	<a class="left carousel-control" href="#carousel-example-generic"
		role="button" data-slide="prev"> <span
		class="glyphicon glyphicon-chevron-left"></span>
	</a> <a class="right carousel-control" href="#carousel-example-generic"
		role="button" data-slide="next"> <span
		class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>