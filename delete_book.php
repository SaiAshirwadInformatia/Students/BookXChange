<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['logged'] ) and empty ( $_SESSION ['logged'] ) == true) {
		header ( "Location: index.php" );
		exit();
}


if(isset($_GET['book_id'])){
$page = "delete_book.php";
require 'hits.php';
$book_id = $_GET ['book_id'];
if(isset($_POST['confirmDelete'])){
	if($_POST['confirmDelete'] == 'Yes'){
		$query = "DELETE  FROM books WHERE id = $book_id";
		if($mysqli->query ( $query )){
			header("Location: profile.php");
			exit();
		}else{
			$_SESSION['error_msg'] = "User deletion failed";
			header("Location: profile.php");
			exit();
		}
	}else{
		header("Location: edit_book.php?book_id=$book_id");
		exit();
	}
}else{

require_once 'inc_header.php'; ?>
<h2>Delete Book</h2><hr>
<form action="" method="POST">
	<div class="form-group">
		<label>Are you sure to delete this Book?</label>
		<button type="submit" name="confirmDelete" value="Yes" class="btn btn-danger">Yes</button>
		<button type="submit" name="confirmDelete" value="No" class="btn btn-success">No</button>
	</div>
</form>
<?php
}
}else{
	header("Location: index.php");
	exit();
}
?>
