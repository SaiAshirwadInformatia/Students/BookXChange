<?php

require_once 'db_connect.php';

$where_array = array ();
if (isset ( $_GET ) and count ( $_GET ) > 1) {
	$fields = array (
			"name" => "string",
			"author" => "string",
			"published" => "string",
			"published_year" => "string",
			"genre_id" => "number" 
	);
	foreach ( $fields as $field => $type ) {
		if (isset ( $_GET [$field] ) and ! empty ( $_GET [$field] )) {
			$_GET[$field] = $mysqli->escape_string($_GET[$field]);
			if ($type == 'string') {
				$where_array [] = " `$field` LIKE '%" . $_GET [$field] . "%' ";
			} else {
				$where_array [] = " `$field` = " . $_GET [$field] . " ";
			}
		}
	}
}
$pass = false;
$query = "SELECT * FROM books";
if (count ( $where_array ) > 0) {
	$query .= " WHERE " . implode ( " AND ", $where_array );
	$pass = true;
}
if ($pass) {
	$query .= " AND id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND is_approved = 1";
} else {
	$query .= " WHERE id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND is_approved = 1";
}
$books = $mysqli->query ( $query );


require_once 'inc_header.php';
$page = "search.php";
require_once 'hits.php';
require_once 'inc_nav.php';
?>
<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li class="active"><span class="glyphicon glyphicon-search"></span>
			Search Books</li>
	</ol>
	<div class="col-md-9">
		<h4>Result</h4>
	</div>
	<div class="col-md-3">
		<h4>Search By</h4>
	</div>
	<hr>
	<hr>
	<div class="col-md-9 booksrow">
					<?php
					if (isset ( $books )) {
						while ( ($book = $books->fetch_assoc ()) != null ) {
							require 'inc_book.php';
						}
						;
					}
					?>
				</div>
	<div class="col-md-3">
		<form action="" method="GET">
			<div>
				<label>Book Name</label> <input type="text" class="form-control"
					name="name"
					value="<?php echo isset($_GET['name'])?$_GET['name']:'';?>" />
			</div>
			<div style="padding-top: 10px">
				<label>Book Author</label> <input type="text" class="form-control"
					name="author"
					value="<?php  echo isset($_GET['author'])?$_GET['author']:'';?>" />
			</div>
			<div style="padding-top: 10px">
				<label>Publisher</label> <input type="text" class="form-control"
					name="published"
					value="<?php  echo isset($_GET['published'])?$_GET['published']:'';?>" />
			</div>
			<div style="padding-top: 10px">
				<label>Published in Year</label> <input type="text"
					class="form-control" name="published_year"
					value="<?php  echo isset($_GET['published_year'])?$_GET['published_year']:'';?>" />
			</div>
			<div style="padding-top: 10px">
				<label>Book Genre</label><?php
				$query = "SELECT * FROM genres";
				$genres = $mysqli->query ( $query );
				if (isset ( $genres ) and $genres->num_rows > 0) {
					
					?> <select name="genre_id" class="form-control">
					<option value="">All</option>
							<?php
					while ( ($genre = $genres->fetch_assoc ()) != null ) {
						echo '<option value="' . $genre ['id'] . '"' . (($genre ['id'] == $_GET ['genre_id']) ? 'selected' : '') . '>' . $genre ['name'] . '</option>';
					}
					?>
        							</select> <?php } ?>					
						
						
						</div>
			<div style="padding-top: 10px">
				<button type="submit" class="btn btn-primary btn-sm">Filter Book</button>
				<a href="search.php" class="btn btn-primary btn-sm">Reset Filter</a>
			</div>

		</form>
	</div>
</div>
</div>

<?php
require_once 'inc_footer.php';
?>
