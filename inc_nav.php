<?php
require_once 'db_connect.php';
?>

<div class="col-md-1 col-xs-1">
	<ul class="nav" id="myTab">
			<?php if(!isset($_SESSION['logged']) or $_SESSION['logged'] != true){?>
	<?php if(strpos($_SERVER['REQUEST_URI'], 'index.php') === false){?>
	<li><a href="index.php" title="Home" class="icon_size"> <span
				class="mdi-action-home"></span>
		</a></li>
	<?php }else {?>
		<li class="active"><a href="index.php" title="Home" class="icon_size">
				<span class="mdi-action-home"></span>
		</a></li>
	<?php }?>
	<?php if (strpos($_SERVER['REQUEST_URI'], 'book_info.php') !== false){?>
	   <li class="active"><a href="books.php" title="Books"
			class="icon_size"> <span class="mdi-av-my-library-books"></span>
		</a></li>
		<?php }else {?>
	<?php if(strpos($_SERVER['REQUEST_URI'], 'books.php') === false){?>
		<li><a href="books.php" title="Books" class="icon_size"> <span
				class="mdi-av-my-library-books"></span>
		</a></li>
	<?php }else {?>
		  <li class="active"><a href="books.php" title="Books"
			class="icon_size"> <span class="mdi-av-my-library-books"></span>
		</a></li>
	<?php }}?>
	<?php if (strpos($_SERVER['REQUEST_URI'], 'search.php') === false) {?>
		<li><a href="search.php" title="Search book" class="icon_size"> <span
				class="mdi-action-search"></span>
		</a></li>
	<?php }else {?>
	<li class="active"><a href="search.php" title="Search book"
			class="icon_size"> <span class="mdi-action-search"></span>
		</a></li>
	<?php }?>
	   <?php if (strpos($_SERVER['REQUEST_URI'], 'login.php') === false ){?>
			<li><a href="index.php" title="Login" class="icon_size"><span
				class="glyphicon glyphicon-log-in"></span> </a></li>
			<?php } else {?>
			<li class="active"><a href="index.php" title="Login"
			class="icon_size"><span class="glyphicon glyphicon-log-in"></span> </a></li>
			<?php }?>
	<?php }else{ ?>	
	<?php if(strpos($_SERVER['REQUEST_URI'], 'index.php') === false){?>
	<li><a href="index.php" title="Home" class="icon_size"> <span
				class="mdi-action-home"></span>
		</a></li>
	<?php }else {?>
		<li class="active"><a href="index.php" title="Home" class="icon_size">
				<span class="mdi-action-home"></span>
		</a></li>
	<?php }?>
		<?php
				if (((strpos ( $_SERVER ['REQUEST_URI'], 'books.php' )) or (strpos ( $_SERVER ['REQUEST_URI'], 'edit_book.php' )) or (strpos ( $_SERVER ['REQUEST_URI'], 'book_info.php' ))) === false) {
					?>
		<li><a href="books.php" title="Books" class="icon_size"> <span
				class="mdi-av-my-library-books"></span>
		</a></li>
	<?php }else {?>
		  <li class="active"><a href="books.php" title="Books"
			class="icon_size"> <span class="mdi-av-my-library-books"></span>
		</a></li>
	<?php }?>
	<?php if (strpos($_SERVER['REQUEST_URI'], 'search.php') === false) {?>
		<li><a href="search.php" title="Search book" class="icon_size"> <span
				class="mdi-action-search"></span>
		</a></li>
	<?php }else {?>
	<li class="active"><a href="search.php" title="Search book"
			class="icon_size"> <span class="mdi-action-search"></span>
		</a></li>
	<?php }?>
	<?php if (strpos($_SERVER['REQUEST_URI'], 'swapped_book.php') === false) {?>
		<li><a href="swapped_book.php" title="My swapped book"
			class="icon_size"> <span class="mdi-action-swap-horiz"></span>
		</a></li>
	<?php }else {?>
	<li class="active"><a href="swapped_book.php" title="My swapped book"
			class="icon_size"> <span class="mdi-action-swap-horiz"></span>
		</a></li>
	<?php }?>
	<?php
				
				if ((strpos ( $_SERVER ['REQUEST_URI'], 'profile.php' ) or strpos ( $_SERVER ['REQUEST_URI'], 'profile_update.php' )) === false) {
					?>
			<li><a href="profile.php" title="Profile" class="icon_size"> <span
				class="mdi-action-perm-identity"></span>
		</a></li>
		<?php } else {?>
		<li class="active"><a href="profile.php" title="Profile"
			class="icon_size"> <span class="mdi-action-perm-identity"></span>
		</a></li>
		<?php }?>
		<li><a href="logout.php" title="Logout" class="icon_size"> <span
				class="mdi-action-exit-to-app"> </span>
		</a></li>
			<?php }?>
		</ul>
</div>
<script>
	$(document).ready(function() {
	    $('a[title]').tooltip();
	});
</script>