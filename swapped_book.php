<?php
require_once 'db_connect.php';

$query = "SELECT * FROM swap where (swapped_user_id = " . $_SESSION ['loggeduser'] ['id'] . " or user_id=" . $_SESSION ['loggeduser'] ['id'] . ") AND is_approved = 1";
$swap_books = $mysqli->query ( $query );

require_once 'inc_header.php';

if (isset ( $_SESSION ['shared'] ) and ! empty ( $_SESSION ['shared'] ) == true) {
	?>
<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<p>Your contact successfully shared.</p>
</div>
<?php
	unset ( $_SESSION ['shared'] );
} else if ($_SESSION ['not_shared'] and ! empty ( $_SESSION ['not_shared'] ) == true) {
	?><div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<p>Your contact successfully shared.</p>
</div><?php
	unset ( $_SESSION ['not_shared'] );
}

require_once 'inc_nav.php';
?>

<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li class="active"><span class="mdi-action-swap-vert"></span> Swapped
			Books</li>
		<li><a href="swap_request.php"><span
				class="glyphicon glyphicon-th-list"></span> Swap Books Request</a></li>
		<li><a href="waiting_swap.php"><span class="glyphicon glyphicon-dashboard"></span> Waiting Swap Books</a></li>
	</ol>
	<h4>Your Swapped Books</h4>
<?php
if (is_object ( $swap_books ) and $swap_books->num_rows > 0) {
	while ( ($swap_book = $swap_books->fetch_assoc ()) != null ) {
		?>
<div class="row">
		<div style="padding-bottom: 20px; padding-top: 20px;">
			<div class="col-md-5 panel panel-primary">
	<?php
		// User and User's Book information
		$query = "SELECT * FROM users WHERE id = " . $swap_book ['user_id'];
		$user = $mysqli->query ( $query );
		$user = $user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $swap_book ['book_id'];
		$book = $mysqli->query ( $query );
		$book = $book->fetch_assoc ();
		
		// Swap User and swap User's Book information
		$query = "SELECT * FROM users WHERE id = " . $swap_book ['swapped_user_id'];
		$swapped_user = $mysqli->query ( $query );
		$swapped_user = $swapped_user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $swap_book ['swapped_book_id'];
		$swapped_book = $mysqli->query ( $query );
		$swapped_book = $swapped_book->fetch_assoc ();
		?><div class="panel-heading">
					<h4 class="panel-title"><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $user ['fname'] . "'s Book";
		}
		?></h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<img alt="<?php echo $book['name']?>"
							src="<?php echo $book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if (($user ['id']) == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-2" align="center">
				<h4 align="center">Swapped Deatils</h4>
				<hr>
				<table class="table table-condensed">
					<tr class="active">
						<td><b>Requested By</b></td>
						<td><?php
		
		if ($user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $user ['fname'] . ' ' . $user ['lname'];
		}
		?></td>
					</tr>
					<tr class="active">
						<td><b>Accepted By</b></td>
						<td><?php
		
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $swapped_user ['fname'] . ' ' . $swapped_user ['lname'];
		}
		?></td>
					</tr>
					<tr class="active">
						<td><b>Swapping Date</b></td>
						<td><?php echo $swap_book['creation_ts'];?></td>
					</tr>
				</table>
			</div>
			<div class="col-md-5 panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">
			<?php
		
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'Your Book';
		} else {
			echo $swapped_user ['fname'] . "'s Book";
		}
		?>
			</h4>
				</div>
				<div class="panel-body">
					<div class="col-md-6" align="center">
						<table class="table table-condensed">
							<tr class="active">
								<td><b>Name</b></td>
								<td><?php echo $swapped_book['name'];?></td>
							</tr>
							<tr class="active">
								<td><b>Added by</b></td>
								<td><?php
		if ($swapped_user ['id'] == $_SESSION ['loggeduser'] ['id']) {
			echo 'You';
		} else {
			echo $swapped_user ['fname'] . ' ' . $swapped_user ['lname'];
		}
		?></td>
							</tr>
							<tr class="active">
								<td><b>Author</b></td>
								<td><?php echo $swapped_book['author'];?></td>
							</tr>
							<tr class="active">
								<td><b>Publisher</b></td>
								<td><?php echo $swapped_book['published'];?></td>
							</tr>
							<tr class="active">
								<td><b>Published Date</b></td>
								<td><?php echo $swapped_book['published_year'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN10</b></td>
								<td><?php echo $swapped_book['isbn10'];?></td>
							</tr>
							<tr class="active">
								<td><b>ISBN13</b></td>
								<td><?php echo $swapped_book['isbn13'];?></td>
							</tr>
							<tr class="active">
								<td><b>Language</b></td>
								<td><?php echo $swapped_book['language'];?></td>
							</tr>
							<tr class="active">
								<td><b>Book Creation Date</b></td>
								<td><?php echo $swapped_book['creation_ts'];?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-6" align="center">
						<img alt="<?php echo $swapped_book['name']?>"
							src="<?php echo $swapped_book['cover_picture'];?>" height="300px"
							width="200px" align="middle">
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
} else {
	?>
<div class="alert alert-dismissable alert-warning">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<p>Oops, You didn't swap your book with another's book</p>
	</div>
<?php
}
?>
</div>