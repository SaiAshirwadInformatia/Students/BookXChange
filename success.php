<?php
require_once 'db_connect.php';

if (! isset ( $_SESSION ['logged'] ) and empty ( $_SESSION ['logged'] ) == true) {
	header ( "Location: index.php" );
	exit ();
}

if (isset ( $_GET ) and ! empty ( $_GET ) == true) {
	$user_id = $_GET ['user_id'];
	$book_id = $_GET ['book_id'];
	$swapped_user_id = $_GET ['swapped_user_id'];
	$swapped_book_id = $_GET ['swapped_book_id'];
	$phone = $_GET ['phone'];
	$access_token = md5 ( $user_id ) . '' . md5 ( time () );
	$query = "INSERT INTO swap (`user_id`, `book_id`, `swapped_user_id`, `swapped_book_id`, `access_token`, user_phone) VALUES";
	$query .= "('$user_id', '$book_id', '$swapped_user_id', '$swapped_book_id' , '$access_token', '$phone')";
	if ($mysqli->query ( $query )) {
		$query = "SELECT * FROM users WHERE id = $user_id";
		$user_name = $mysqli->query ( $query );
		$user_name = $user_name->fetch_assoc ();
		
		$query = "SELECT * FROM users WHERE id = $swapped_user_id";
		$swapped_user_name = $mysqli->query ( $query );
		$swapped_user_name = $swapped_user_name->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = $book_id";
		$user_book = $mysqli->query ( $query );
		$user_book = $user_book->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = $swapped_book_id";
		$swapped_user_book = $mysqli->query ( $query );
		$swapped_user_book = $swapped_user_book->fetch_assoc ();
		
		$query = "SELECt * FROM swap WHERE access_token = '$access_token'";
		$swap = $mysqli->query ( $query );
		$swap = $swap->fetch_assoc ();
		
		$mail->addAddress ( $swapped_user_name ['email'], $swapped_user_name ['fname'] . ' ' . $swapped_user_name ['lname'] );
		$message = file_get_contents ( 'templates/swapped_book_user.txt' );
		$message = str_replace ( "*|USER_NAME|*", $user_name ['fname'], $message );
		$message = str_replace ( "*|USER_BOOK|*", $user_book ['name'], $message );
		$message = str_replace ( "*|SWAPPED_USER_NAME|*", $swapped_user_name ['fname'], $message );
		$message = str_replace ( "*|SWAPPED_USER_BOOK|*", $swapped_user_book ['name'], $message );
		$message = str_replace ( "*|ACCESS_TOKEN|*", $swapped_user_name ['access_token'], $message );
		$message = str_replace ( "*|SWAP_AT|*", $swap ['access_token'], $message );
		$mail->Subject = "Book Swapping Notification - BookXchange";
		$message = nl2br ( $message );
		$mail->msgHTML ( $message );
		
		if ($mail->send ()) {
		} else {
			echo $mail->ErrorInfo;
		}
		
		$mail->clearAddresses ();
		$mail->addAddress ( $user_name ['email'], $user_name ['fname'] . ' ' . $user_name ['lname'] );
		$message = file_get_contents ( 'templates/book_user.txt' );
		$message = str_replace ( "*|USER_NAME|*", $user_name ['fname'], $message );
		$message = str_replace ( "*|USER_BOOK|*", $user_book ['name'], $message );
		$message = str_replace ( "*|SWAPPED_USER_BOOK|*", $swapped_user_book ['name'], $message );
		$message = nl2br ( $message );
		$mail->msgHTML ( $message );
		$mail->Subject = "Book Swapping Notification - BookXchange";
		if ($mail->send ()) {
		} else {
			echo $mail->ErrorInfo;
		}
	}
} else {
	header ( "Location: index.php" );
}
$page = "success.php";
require_once 'hits.php';

require_once 'inc_header.php';
require_once 'inc_nav.php';

?>
<div class="col-md-11">
	<div class="jumbotron">
		<h4>
			Thank You for swapping this book, we have notified the book
			<code>owner</code>
			for swapping of book.
		</h4>
		<h4>
			If the owner of book, approves you will be notified with his
			<code>contact details.</code>
		</h4>
	</div>
</div>
