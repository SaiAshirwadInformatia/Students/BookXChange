<?php
if (! isset ( $_SESSION ['logged'] ) and empty ( $_SESSION ['logged'] ) == true) {
	$_SESSION ['logged_msg'] = '<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Please Login!</strong>
</div>';
}
?>
<div class="col-md-3 bookcolumn col-xs-12">
	<div class="thumbnail" style="margin-top: 10px">
		<h4 style="border-bottom: 1px solid">
			<a href="book_info.php?book_id=<?php echo $book['id'];?>"
				title="<?php echo $book['name'];?>"><?php
				if (strpos ( $_SERVER ['REQUEST_URI'], 'search.php' ) == true) {
					echo truncate ( $book ['name'], 13 );
				} else {
					echo truncate ( $book ['name'], 22 );
				}
				?></a> <?php
				
				if (! isset ( $_SESSION ['loggeduser'] ['id'] ) or $_SESSION ['loggeduser'] ['id'] != $book ['user_id']) {
				} else {
					?><sup><a href="edit_book.php?book_id=<?php echo $book['id'];?>"
				class="pull-right" title="Update Book"><span class="mdi-image-edit"></span></a></sup><?php }?>
		</h4>
		<a href="book_info.php?book_id=<?php echo $book['id'];?>"> <img
			src="<?php
			
if (isset ( $book ['cover_picture'] ) and file_exists ( $book ['cover_picture'] )) {
				echo $book ['cover_picture'];
			} else {
				echo "images/book_image1.jpg";
			}
			?>"
			style="width: 180px; height: 240px;"
			title="<?php echo $book['name'];?>" alt="<?php echo $book['name'];?>" /></a>
		<?php
		
		if (! isset ( $_SESSION ['logged'] ) or (isset ( $_SESSION ['logged'] ) and $_SESSION ['loggeduser'] ['id'] != $book ['user_id'])) {
			?>
	       <a href="swap.php?swapped_book_id=<?php echo $book['id']?>"
			class="btn btn-primary btn-block " style="margin-top: 10px">Swap This</a>
			<?php
		} else {
			echo '<a href="book_info.php?book_id=' . $book ['id'] . '" class="btn btn-primary btn-block" style="margin-top:10px">Check Your Book</a>';
		}
		?>
	</div>
</div>
