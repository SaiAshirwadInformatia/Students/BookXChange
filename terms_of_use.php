<?php
require_once 'inc_header.php';

$page = "terms_of_use.php";
require_once 'hits.php';

require_once 'inc_nav.php';

?>


<div class="content_wrap col-md-11">
	<h3>Terms of use</h3>
	<ol>
		<li><h5>IMPORTANT INFORMATION</h5>
			<p>You should carefully read the following Terms and Conditions. Your
				purchase or use of our products implies that you have read and
				accepted these Terms and Conditions.</li>
		<li><h5>LICENSE</h5>
			<p>Our website grants you three possible types of license to use the
				web templates and other products (the "products") sold through our
				website by independent content providers in accordance with these
				Terms and Conditions (the "license") issued by our company, as
				follows:</p>
			<ol>
				<li><h5>ONE TIME USAGE LICENSE</h5>
					<p>You may be granted a One Time Usage License in case of
						purchasing a website template at a Non-Unique Price. It enables
						you to use each individual product on a single website only,
						belonging to either you or your client. You have to purchase the
						same template again if you to use the same design in connection
						with another or other projects;</li>
				<li><h5>DEVELOPERS LICENSE</h5>
					<p>You may be granted a Developers License entitles you to make
						some modifications with the products using any software or
						applications and TemplateMonster designs. You are permitted to
						redistribute or resell your projects after the receiving the
						license. You can apply for the license at email protect.</li>
				<li><h5>BUYOUT PURCHASE LICENSE</h5>
					<p>You may be granted a Buyout Purchase License in case of
						purchasing a website template at a Buyout Purchase Price. This
						type of license guarantees that you are the last person to buy
						this template. After the buyout purchase occurs, the template is
						permanently removed from the TemplateMonster sales directory and
						is never available to other customers again. You can not
						redistribute or resell templates after Buyout Purchase Price.</li>
			</ol></li>
		<li><h5>IMAGERY, CLIPARTS AND FONTS</h5>
			<p>
				All imagery, clipart, fonts and video footages used in our products
				are royalty-free and are the integral part of our products. One Time
				Usage License and Developers License give you the right to use
				images, clipart, fonts and video footages only as a part of the
				website you build using your template. You can use imagery, clipart,
				fonts and video footages to develop one project only. Any kind of
				separate usage or distribution is strictly prohibited. All images
				and illustrations used in templates come in a single layer, as is.<br />
				Some of the templates that are animated with the Flash technology
				may contain effects created with the following software packages:
				Adobe AfterEffects, 3DMax. To clarify whether or not effects like
				these are present in certain template please address our Support
				Team for further consulting.<br /> Please be informed that for
				editing these effects you will need proper software and skills. The
				alternate variant is to hire someone to do it for you. We recommend
				that you address TemplateTuning.com for this.</li>
		<li><h5>MODIFICATIONS</h5>
			<p>You are authorized to make necessary modification(s) to our
				products to fit your purposes in accordance with the type of license
				you acquire.</li>
		<li><h5>UNAUTHORIZED USE</h5>
			<p>If you have not received the Buyout License, you shall not place
				any of our products, modified or unmodified, on a diskette, CD,
				website or any other medium. You also shall not offer them for
				redistribution or resale of any kind without prior written consent
				from our company.</li>
		<li><h5>ASSIGNABILITY</h5>
			<p>You shall not sub-license, assign, or transfer the any mentioned
				above to any entity without prior written consent from our company.</li>
		<li><h5>OWNERSHIP</h5>
			<p>You do not claim intellectual property right or exclusive
				ownership to any of our products, modified or unmodified. All
				products are property of independent content providers. Our products
				are provided &#8220;as is&#8221; without warranty of any kind,
				either expressed or implied. In no event shall our company or its
				agents be liable for any damages including, but not limited to,
				direct, indirect, special, punitive, incidental or consequential, or
				other losses arising out of the use of or inability to use our
				products.</li>
		<li><h5>INSTALLATIONS</h5>
			<p>We do not install any of our products which require this
				(osCommerce templates, Zen Cart templates, etc.). Installation is a
				paid service which is not included in the package price.</li>
		<li><h5>THIRD-PARTIES SERVICES</h5>
			<p>We may allow access to or advertise certain third-party product or
				service providers (&#8220;Merchants&#8221;) from which you may
				purchase certain goods or services. You understand that we do not
				operate or control the products or services offered by Merchants.
				Merchants are responsible for all aspects of order processing,
				fulfillment, billing and customer service. We are not a party to the
				transactions entered into between you and Merchants. You agree that
				use of or purchase from such Merchants is at your sole risk and is
				without warranties of any kind by us, expressed, implied or
				otherwise including warranties of title, fitness for purpose,
				merchantability or non-infringement. under no circumstances are we
				liable for any damages arising from the transactions between you and
				merchants or for any information appearing on merchant sites or any
				other site linked to our site.</li>
		<li><h5>REGISTRATION/PURCHASE</h5>
			<p>Purchasing from the Site may optionally require you to register.
				If registration is performed, you agree to provide us with accurate,
				complete registration and/or purchase information. Your registration
				must be done using accurate information. Each registration is for
				your personal use only. We do not permit:</p>
			<ol>
				<li>any other person using the registered sections under your name;
					or</li>
				<li>access through a single name being made available to multiple
					users on a network. You are responsible for preventing such
					unauthorized use.</li>
			</ol>
			<p>Templatemonster.com retains the sole right and authority to accept
				or reject your registration, or to terminate registration once
				accepted, for any reason that it deems appropriate.</li>
		<li><h5>ERRORS, CORRECTIONS AND CHANGES</h5>
			<p>We do not represent or warrant that the Site will be error-free,
				free of viruses or other harmful components, or that defects will be
				corrected. We do not represent or warrant that the information
				available on or through the Site will be correct, accurate, timely
				or otherwise reliable. We may make to the features, functionality or
				content of the Site at any time. We reserve the right in our sole
				discretion to edit or delete any documents, information or other
				content.</li>
		<li><h5>THIRD PARTY CONTENT</h5>
			<p>Third party content may appear on the Site or may be accessible
				via links from the Site. We are not responsible for and assume no
				liability for any mistakes, misstatements of law, defamation,
				omissions, falsehood, obscenity, pornography or profanity in the
				statements, opinions, representations or any other form of content
				on the Site. You understand that the information and opinions in the
				third party content represent solely the thoughts of the author and
				is neither endorsed by nor does it necessarily reflect our belief.</li>
		<li><h5>UNLAWFUL ACTIVITY</h5>
			<p>We reserve the right to investigate complaints or reported
				violations of this Agreement and to take any action we deem
				appropriate, including but not limited to reporting any suspected
				unlawful activity to law enforcement officials, regulators, or other
				third parties and disclosing any information necessary or
				appropriate to such persons or entities relating to your profile,
				email addresses, usage history, posted materials, IP addresses and
				traffic information.</li>
		<li><h5>LIMITATION OF LIABILITY</h5>
			<ol>
				<li><p>We and any Affiliated Party shall not be liable for any loss,
						injury, claim, liability, or damage of any kind resulting in any
						way from</p>
					<ol>
						<li>any errors in or omissions from the Site or any services or
							products obtainable therefrom,</li>
						<li>the unavailability or interruption of the Site or any features
							thereof,</li>
						<li>your use of the Site,</li>
						<li>the content contained on the Site, or</li>
						<li>any delay or failure in performance beyond the control of a
							Covered Party.</li>
					</ol></li>
				<li>the aggregate liability of us and the affiliated parties in
					connection with any claim arising out of or relating to the site
					and/or the products, information, documents and services provided
					herein or hereby shall not exceed $100 and that amount shall be in
					lieu of all other remedies which you may have against us and any
					affiliated party.</li>
			</ol></li>
	</ol>
	<p class="text-center">
		<strong>Our company reserves the right to change or modify the terms
			and conditions without prior notice.</strong>
	</p>
</div>




<?php

require_once 'inc_footer.php';

?>