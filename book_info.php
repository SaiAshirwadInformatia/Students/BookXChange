<?php
require_once 'db_connect.php';
if (isset ( $_GET ['book_id'] )) {
	$book_id = $_GET ['book_id'];
	$query = "SELECT * FROM books WHERE  id  = $book_id";
	$books = $mysqli->query ( $query );
	
	if (! is_null ( $books )) {
		$book = $books->fetch_assoc ();
		$query = "SELECT * FROM users WHERE id = " . $book ['user_id'];
		$users = $mysqli->query ( $query );
		$book_user= $users->fetch_assoc ();
		$query = "SELECT * FROM genres WHERE id = " . $book ['genre_id'];
		$genre = $mysqli->query ( $query );
		$genre = $genre->fetch_assoc ();
	}
	$query = "SELECT * FROM books WHERE id NOT IN (SELECT book_id FROM swap) AND id NOT IN (SELECT swapped_book_id FROM swap) AND id != " . $_GET ['book_id'] . " AND is_approved = 1  ORDER BY rand() LIMIT 0,4";
	$books = $mysqli->query ( $query );
}


require_once 'inc_header.php';

$page = "books.php";
require_once 'hits.php';
require_once 'inc_nav.php';
?>
<div class="col-md-11 col-xs-11">
	<div class="row" style="margin-top: 10px">
	<?php
	
	if ((! isset ( $_SESSION ['logged'] ) or isset ( $_SESSION ['logged'] )) and isset ( $_SESSION ['loggeduser'] ) and $book ['user_id'] == $_SESSION ['loggeduser'] ['id']) {
		?>
		<div class="col-md-12 col-xs-12">
		<?php }else{?>
		<div class="col-md-8 col-xs-7">
		<?php }?>
			<h4>
					<?php echo $book['name'];?> Added By <a
						href="profile.php?user_id=<?php echo $book_user['id'];?>"><?php
						echo $book_user['fname'] . ' ' . $book_user['lname'];
						?></a>
			<?php
			
			if ((! isset ( $_SESSION ['logged'] ) or isset ( $_SESSION ['logged'] )) and isset ( $_SESSION ['loggeduser'] ) and $book ['user_id'] == $_SESSION ['loggeduser'] ['id']) {
				echo '<a href="edit_book.php?book_id=' . $book ['id'] . '" title="Update Book" class="btn btn-primary btn-xs pull-right"><span class="mdi-image-edit"></span></a>';
			}
			?></h4>
			</div>
			<div class="col-md-4 col-xs-5"><?php
			if ((! isset ( $_SESSION ['logged'] ) or isset ( $_SESSION ['logged'] )) and isset ( $_SESSION ['loggeduser'] ) and $book ['user_id'] != $_SESSION ['loggeduser'] ['id']) {
				?>
			<a class="btn btn-primary btn-block"
					href="swap.php?swapped_book_id=<?php echo $book['id'];?>"
					style="margin-bottom: 10px">Swap This Now </a>
<?php } ?>
		</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-4 col-xs-4">

				<img src="<?php echo $book['cover_picture']?>"
					class="img-thumbnail img-responsive" alt=""
					style="max-height: 250px" title="<?php echo $book['name'];?>">

			</div>
			<div class="col-md-8">

				<table>
					<tr>
						<td>Genre</td>
						<td>:</td>
						<td><?php echo  $genre['name']  ; ?></td>
					</tr>
					<tr>
						<td>Author</td>
						<td>:</td>
						<td><?php echo  $book['author']  ; ?></td>
					</tr>
					<tr>
						<td>Pages</td>
						<td>:</td>
						<td><?php echo  $book['pages']  ;?></td>
					</tr>
					<tr>
						<td>Price</td>
						<td>:</td>
						<td><?php  echo $book['price'];?></td>
					</tr>
					<tr>
						<td>ISBN 13</td>
						<td>:</td>
						<td><?php echo  $book['isbn13']  ; ?></td>
					</tr>
					<tr>
						<td>ISBN 10</td>
						<td>:</td>
						<td><?php echo  $book['isbn10']  ; ?></td>
					</tr>
				</table>
				<h4>Description :</h4>
				<div class="thumbnail" style="height: relative" name="description">
					<p><?php  echo $book['description']?></p>
				</div>
			</div>

		</div>
		<div class="row">
<?php
while ( ($book = $books->fetch_assoc ()) != null ) {
	require 'inc_book.php';
}
?></div>
	</div>
</div>
<?php
require_once 'inc_footer.php';

?>
