<?php


require_once 'db_connect.php';

if (! isset ( $_SESSION ['logged'] ) and empty ( $_SESSION ['logged'] ) == true) {
	header ( "Location: index.php" );
	exit ();
}



if (isset ( $_POST ) and ! empty ( $_POST ) == true) {
	if (($_POST ['new_pass'] == $_POST ['confirm_pass']) == true) {
		if (isset ( $_POST ['current_pass'] ) and ! empty ( $_POST ['current_pass'] ) == true) {
			if (isset ( $_POST ['new_pass'] ) and ! empty ( $_POST ['new_pass'] ) == true) {
				if (isset ( $_POST ['confirm_pass'] ) and ! empty ( $_POST ['confirm_pass'] ) == true) {
					$query = "SELECT * FROM users WHERE password = '" . md5 ( $_POST ['current_pass'] ) . "' AND id = " . $_SESSION ['loggeduser'] ['id'];
					$password = $mysqli->query ( $query );
					if (is_object ( $password ) and $password->num_rows > 0 and ! empty ( $password )) {
						$query = "UPDATE users SET password = '" . md5 ( $_POST ['confirm_pass'] ) . "' ";
						$query .= "WHERE password = '" . md5 ( $_POST ['current_pass'] ) . "' AND id = " . $_SESSION ['loggeduser'] ['id'];
						if ($mysqli->query ( $query )) {
							$user = $password->fetch_assoc ();
							$mail->addAddress ( $user ['email'], $user ['fname'] . ' ' . $user ['lname'] );
							$message = file_get_contents ( "templates/change_password.txt" );
							$message = str_replace ( "*|FNAME|*", $user ['fname'], $message );
							$message = str_replace ( "*|PASSWORD|*", $_POST ['confirm_pass'], $message );
							$message = nl2br ( $message );
							$mail->msgHTML ( $message );
							$mail->Subject = "Change Password - BookXchange";
							if (! $mail->send ()) {
								echo $mail->ErrorInfo;
							}
							echo '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Password Update Successfully</strong>
								</div>';
						} else {
							echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Updation Failed</strong>
								</div>';
						}
					} else {
						echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Please Enter Correct Password</strong>
								</div>';
					}
				} else {
					echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Please Enter Confirm Passsword</strong>
								</div>';
				}
			} else {
				echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Please Enter New Password</strong>
								</div>';
			}
		} else {
			echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Please Enter Current password</strong>
								</div>';
		}
	} else {
		echo '<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>Your confirm password is not match with your new password</strong>
								</div>';
	}
}

require_once 'inc_header.php';
$page = "change_password.php";
require_once 'hits.php';
require_once 'inc_nav.php';

?>

<div class="col-md-11">
	<ol class="breadcrumb">
		<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>
				Home</a></li>
		<li><a href="profile.php"><span class="glyphicon glyphicon-user"></span>
				Profile <span class="glyphicon glyphicon-arrow-right"></span> <?php echo $user['fname'];?></a></li>
		<?php if(!isset($_GET['user_id'])){?>
		<li class="active"><span class="glyphicon glyphicon-cog"></span>
			Change Password</li>
				<?php }?>
	</ol>
	<div class="row">
		<h3>Change Password</h3>
		<hr>
		<div class="col-md-4">
			<form action="" method="POST">
				<div class="col-md-12">
					<div class="form-group">
						<label> Current password: </label> <input type="password"
							class="form-control" name="current_pass" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label> New password: </label> <input type="password"
							class="form-control" name="new_pass"
							data-hint="Password should be minimun 6 charcters" />
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label> Confirm password: </label> <input type="password"
							class="form-control" name="confirm_pass" />
					</div>
				</div>
				<div class="col-md-offset-6 col-md-6">
					<button class="btn btn-success btn-block" type="submit"
						style="margin-top: 10px">Change Password</button>
				</div>
			</form>
		</div>
	</div>
</div>