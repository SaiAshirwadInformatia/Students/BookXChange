<?php
require_once 'db_connect.php';

?>
<!DOCTYPE html>
<!--
----------------------------------------------------------------------------
Developed and Maintained by Sai Ashirwad Informatia
----------------------------------------------------------------------------
----------------------------------------------------------------------------
Authors
----------------------------------------------------------------------------
Akshay Mane         akmane29@gmail.com
Kunal Band          kunalbund1996@gmail.com
----------------------------------------------------------------------------
-->
<html>
<head>
<title>Bookxchange</title>
<link rel="icon" href="images/book_image1.jpg" type="image/jpg" />
<link rel="stylesheet" href="style.css" />
<link rel="stylesheet" type="bower_components/socjs/soc.min.css">
<link rel="stylesheet"
	href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
<link
	href="bower_components/bootstrap-material-design/dist/css/roboto.min.css"
	rel="stylesheet">
<link
	href="bower_components/bootstrap-material-design/dist/css/material.min.css"
	rel="stylesheet">
<link
	href="bower_components/bootstrap-material-design/dist/css/ripples.min.css"
	rel="stylesheet">


<link
	href="bower_components/bootstrap-material-design/dist/css/material-fullpalette.css"
	rel="stylesheet">
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script
	src="bower_components/bootstrap-material-design/dist/js/material.min.js"></script>
<script
	src="bower_components/bootstrap-material-design/dist/js/ripples.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="bower_components/tinymce/tinymce.min.js"></script>
<!-- <script src="bower_components\blocksit-js\blocksit.min.js"></script> -->
</head>

<body class="body">
	<script>
            $(function() {
                $.material.init();
            });
        </script>
	<div class="container-fluid">
	<div class="header-img jumbotron"
		style="padding-top: 0px;">
	<?php if(!isset($_SESSION['logged']) or $_SESSION['logged'] != true){?>
		<form action="login.php" method="POST">
			<div class="row">
				<div class="col-md-7 col-xs-12">
					<img alt="BookXchange" src="images/BookXchange.png"
						style="padding-top: 22px;">
				</div>
				<div class="col-md-2 form-group col-xs-7">
					<label class="form-control">Username or Email ID</label> <input
						type="text" class="form-control" name="email_username"
						style="border: 2px solid;"<?php if(isset($_SESSION['login_click'])){?> autofocus="autofocus"<?php unset($_SESSION['login_click']);}?> />
				</div>
				<div class="col-md-2 form-group">
					<label class="form-control">Password</label> <input type="password"
						class="form-control" name="password" style="border: 2px solid;" />
					<?php if(!strpos($_SERVER['REQUEST_URI'], 'recover_password')){?><a href="recover_password.php" class="pull-right"
						style="color: black; margin-top: 6px;">Recover Password</a><?php }?>
				</div>
				<div class="col-md-1" align="center">
					<label><a href="register.php" style="color: black"></a></label>
					<button class="btn btn-success" type="submit"
						style="margin-top: 31px; margin-left: 0px; margin-bottom: 0px; padding: 8px">Login</button>
					<?php if(strpos($_SERVER['REQUEST_URI'], 'register.php') === false){?><a href="register.php" style="color: black">Register</a>
				<?php }?></div>
			</div>
		</form>
		<?php if(strpos($_SERVER['REQUEST_URI'], 'recover_password') === false){?>
		<div class="col-md-offset-10"></div>
		<?php
		}
	} else {
		$user = $_SESSION ['loggeduser'];
		$user_id = $user ['id'];
		$query = "SELECT * FROM users WHERE id = $user_id";
		$user = $mysqli->query ( $query );
		$user = $user->fetch_assoc ();
		?>
		<div class="row">
			<div class="col-md-7 col-xs-6">
				<img alt="BookXchange" src="images/BookXchange.png"
					style="padding-top: 22px;">
			</div>
			<div class="col-md-5">
				<div class="col-md-offset-4 col-md-6 pull-right">
					<b>Welcome <?php echo $user['fname'];?> </b><a href="logout.php"
						class="btn btn-danger btn-xs" style="margin-top: 1px;"><span
						class="glyphicon glyphicon-off"></span> Logout</a>
				</div><!--  
				<div class="col-md-12">
					<div class="form-group col-md-5">
						<div class="col-md-10">
							<input class="form-control" type="text" name="name"
								data-hint="Enter the name of the book"
								style="margin-top: 10px; border: 2px solid;" />
						</div>
						<div class="col-md-2">
							<button class="btn btn-primary btn-xs">
								<span class="mdi-action-find-in-page"></span>
							</button>
						</div>
					</div>
					<div class="col-md-offset-2 col-md-5">
						<a href="edit_book.php"
							class="btn btn-success btn-xs form-control"><span
							class="glyphicon glyphicon-book"></span>Add Book</a>
					</div>
				</div> -->
			</div>
		</div>
	<?php 	} ?>
		
	</div>