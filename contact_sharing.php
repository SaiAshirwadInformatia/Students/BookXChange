<?php
require_once 'db_connect.php';

if (isset ( $_SESSION ['looged'] ) and ! empty ( $_SESSION ['logged'] ) == true) {
	header ( "Location: index.php" );
	exit ();
}

$_SESSION ['shared'] = false;

if (isset ( $_GET ['swap_at'] ) and ! empty ( $_GET ['swap_at'] ) == true) {
	$query = "SELECT * FROM swap WHERE access_token = '" . $_GET ['swap_at'] . "' and is_approved = 0";
	$swaps = $mysqli->query ( $query );
	if (is_object ( $swaps ) and $swaps->num_rows > 0) {
		$swap = $swaps->fetch_assoc ();
		
		$swapped_user_id = $swap ['swapped_user_id'];
		$swapped_book_id = $swap ['swapped_book_id'];
		
		$user_id = $swap ['user_id'];
		$book_id = $swap ['book_id'];
		
		// user
		$query = "SELECT * FROM users WHERE id = " . $user_id;
		$users = $mysqli->query ( $query );
		$users = $users->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $book_id;
		$book = $mysqli->query ( $query );
		$book = $book->fetch_assoc ();
		
		// Swap User and swap User's Book information
		$query = "SELECT * FROM users WHERE id = " . $swapped_user_id;
		$swapped_user = $mysqli->query ( $query );
		$swapped_user = $swapped_user->fetch_assoc ();
		
		$query = "SELECT * FROM books WHERE id = " . $swapped_book_id;
		$swapped_book = $mysqli->query ( $query );
		$swapped_book = $swapped_book->fetch_assoc ();
	}
	if (isset ( $_POST ['phone'] )) {
		if (strlen ( $_POST ['phone'] ) > 9) {
			$swapped_user_phone = $_POST ['phone'];
			$_SESSION ['shared'] = true;
		} else {
			$_SESSION ['Invalid'] = true;
		}
	}
	if (isset ( $_SESSION ['shared'] ) and ! empty ( $_SESSION ['shared'] ) == true) {
		$query = "UPDATE swap SET is_approved = 1, swapped_user_phone = '" . $swapped_user_phone . "' WHERE access_token = '" . $_GET ['swap_at'] . "'";
		if (! $mysqli->query ( $query )) {
			$_SESSION ['shared'] != true;
			$_SESSION ['not_shared'] = true;
		} else {
			$mail->addAddress ( $swapped_user ['email'], $swapped_user ['fname'] . ' ' . $swapped_user ['lname'] );
			$message = file_get_contents ( 'templates/swapped_user_confirmation_mail.txt' );
			$message = str_replace ( "*|SWAPPED_USER_EMAIL_ID|*", $swapped_user ['email'], $message );
			$message = str_replace ( "*|SWAPPED_USER_PHONE|*", $swapped_user_phone, $message );
			$message = str_replace ( "*|SWAPPED_USER_NAME|*", $swapped_user ['fname'], $message );
			$message = str_replace ( "*|USER_NAME|*", $users ['fname'], $message );
			$message = str_replace ( "*|USER_EMAIL_ID|*", $users ['email'], $message );
			$message = str_replace ( "*|BOOK_NAME|*", $book ['name'], $message );
			$message = str_replace ( "*|USER_PHONE|*", $swap ['user_phone'], $message );
			$mail->Subject = "Contact Details for Swapping - BookXchange";
			$message = nl2br ( $message );
			$mail->msgHTML ( $message );
			
			if ($mail->send ()) {
			} else {
				echo $mail->ErrorInfo;
			}
			
			$mail->clearAddresses ();
			$mail->addAddress ( $users ['email'], $users ['fname'] . ' ' . $users ['lname'] );
			$message = file_get_contents ( 'templates/user_confirmation_mail.txt' );
			$message = str_replace ( "*|SWAPPED_USER_EMAIL_ID|*", $swapped_user ['email'], $message );
			$message = str_replace ( "*|SWAPPED_USER_PHONE|*", $swapped_user_phone, $message );
			$message = str_replace ( "*|SWAPPED_USER_NAME|*", $swapped_user ['fname'], $message );
			$message = str_replace ( "*|USER_NAME|*", $users ['fname'], $message );
			$message = str_replace ( "*|USER_EMAIL_ID|*", $users ['email'], $message );
			$message = str_replace ( "*|BOOK_NAME|*", $book ['name'], $message );
			$message = str_replace ( "*|USER_PHONE|*", $swap ['user_phone'], $message );
			$message = nl2br ( $message );
			$mail->msgHTML ( $message );
			$mail->Subject = "Contact Details for Swapping - BookXchange";
			if ($mail->send ()) {
			} else {
				echo $mail->ErrorInfo;
			}
		}
		header ( "Location: swapped_book.php" );
		exit ();
	}
	
	require_once 'inc_header.php';
	if (isset ( $_SESSION ['Invalid'] ) and ! empty ( $_SESSION ['Invalid'] ) == true) {
		?>
<div class="alert alert-dismissable alert-warning">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<p>Please enter valid phone number.</p>
</div>
<?php
		unset ( $_SESSION ['Invalid'] );
	}
	
	require_once 'inc_nav.php';
	?>
<div class="col-md-11">
	<div class="col-md-8">
		<div class="jumbotron">
			<form action="" method="POST">
				<p>You need to share your contact with <?php echo $users['fname'] .' '.$users['lname'];?></p>
				<div class="col-md-4">
					<input type="number" name="phone" class="form-control"
						placeholder="(ex: 9876543210)" />
				</div>
				<div class="col-md-offset-2">
					<p>
						<button class="btn btn-primary btn-sm" type="submit">Share</button>
					</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
} else if (isset ( $_GET ['user_id'] ) and ! empty ( $_GET ['user_id'] )) {
	if (isset ( $_POST ['phone'] )) {
		if (strlen ( $_POST ['phone'] ) > 9) {
			$_SESSION ['shared'] = true;
			$user_id = $_GET ['user_id'];
			$book_id = $_GET ['book_id'];
			$swapped_user_id = $_GET ['swapped_user_id'];
			$swapped_book_id = $_GET ['swapped_book_id'];
			$phone = $_POST ['phone'];
			header ( "Location: waiting_swap.php?user_id=$user_id&book_id=$book_id&swapped_user_id=$swapped_user_id&swapped_book_id=$swapped_book_id&phone=$phone" );
			exit ();
		} else {
			$_SESSION ['Invalid'] = true;
		}
	}
	$swapped_user_id = $_GET ['swapped_user_id'];
	$query = "SELECT * FROM users WHERE id = $swapped_user_id";
	$swapped_user = $mysqli->query ( $query );
	$swapped_user = $swapped_user->fetch_assoc ();
	require_once 'inc_header.php';
	if (isset ( $_SESSION ['shared'] ) and ! empty ( $_SESSION ['shared'] ) == true) {
		header ( "Location: waiting_swap.php" );
		exit ();
	} else if (isset ( $_SESSION ['Invalid'] ) and ! empty ( $_SESSION ['Invalid'] ) == true) {
		?>
<div class="alert alert-dismissable alert-warning">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<p>Please enter valid phone number.</p>
</div>
<?php
		unset ( $_SESSION ['Invalid'] );
	}
	require_once 'inc_nav.php';
	?>
<div class="col-md-11">
	<div class="col-md-8">
		<div class="jumbotron">
			<form action="" method="POST">
				<p>You need to share your contact with <?php echo $swapped_user['fname'] .' '.$swapped_user['lname'];?></p>
				<div class="col-md-4">
					<input type="number" name="phone" class="form-control"
						placeholder="(ex: 9876543210)" />
				</div>
				<div class="col-md-offset-2">
					<p>
						<button class="btn btn-primary btn-sm" type="submit">Share</button>
					</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
} else {
	header ( "Location: index.php" );
}
?>