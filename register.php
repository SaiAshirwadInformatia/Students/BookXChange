<?php
require_once 'db_connect.php';

if (isset ( $_POST ) and count ( $_POST ) > 0) {
	$fields = array (
			"fname",
			"lname",
			"email",
			"password",
			"username",
			"gender"
	);
	$isValid = true;
	$pass_length = true;
	foreach ( $fields as $field ) {
		if (! isset ( $_POST [$field] ) or empty ( $_POST [$field] )) {
			$isValid = ! $isValid;
			break;
		} else {
			if ($field == 'password') {
				$pass_length = strlen ( $_POST ['password'] );
				if (strlen ( $_POST ['password'] ) >= 6) {
					$pass = $_POST ['password'];
					$$field = md5 ( $_POST [$field] );
					$pass_length = true;
				} else {
					$pass_length = false;
				}
			} else {
				$$field = $_POST [$field];
			}
		}
	}
	if (isset ( $_POST ['terms_to_agree'] )) {
		if ($pass_length) {
			if ($isValid) {
				$access_token = md5 ( $username . time () . $email );
				$query = "INSERT INTO users (fname, lname, email, password, username, gender, access_token) ";
				$query .= "VALUES ('$fname', '$lname', '$email', '$password', '$username', '$gender', '$access_token')";
				if ($mysqli->query ( $query )) {
					$mail->addAddress ( $email, $fname . ' ' . $lname );
					$message = file_get_contents ( 'templates/successful_register.txt' );
					$message = str_replace ( "*|FNAME|*", $fname, $message );
					$message = str_replace ( "*|USERNAME|*", $username, $message );
					$message = str_replace ( "*|PASSWORD|*", $pass, $message );
					$message = str_replace ( "*|ACCESS_TOKEN|*", $access_token, $message );
					$message = nl2br ( $message );
					$mail->msgHTML ( $message );
					$mail->Subject = "Successfully registerd - BookXchange";
					if ($mail->send ()) {
						header ( "Location: index.php" );
						exit ();
					} else {
						echo "$mail->ErrorInfo";
					}
				} else {
					?>
<div class="alert alert-warning alert-dismissible" role="alert"><button>
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Your Email ID / Username is already exist !</strong>
</div>
<?php
				}
			} else {
				?>
<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Please fill all the required field</strong>
</div>
<?php
			}
		} else {
			?>
<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>Paswword strength is very weak</strong>
</div>
<?php
		}
	} else {
		?>
<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
	</button>
	<strong>You need to agree to our terms and condition before registering</strong>
</div>
<?php
	}
}

require_once 'inc_header.php';

$page = "register.php";

require_once 'hits.php';
?>
<div class="container">
	<div class="col-md-10 col-md-offset-1 form-image">
		<form class="form-horizontal" action="" method="POST">
			<h2 align="center">Registration Form</h2>
			<div class="row">
				<div class="col-md-6">
					<label for="fname">Firstname</label> <input type="text"
						name="fname" class="form-control" placeholder="(ex: John)"
						value="<?php echo isset($_POST['fname']) ? $_POST['fname']:'';?>" />
				</div>
				<div class="col-md-6">
					<label for="lname">Lastname</label> <input type="text"
						class="form-control" name="lname" placeholder="(ex: Smith)"
						value="<?php echo isset($_POST['lname']) ? $_POST['lname']:'';?>" />
				</div>
			</div>
			<div class="row" style="padding-top: 10px">
				<div class="col-md-6">
					<label for="email">Email ID</label> <input type="email"
						class="form-control" name="email"
						placeholder="johnsmith@gmail.com"
						value="<?php echo isset($_POST['email']) ? $_POST['email']:'';?>" data-hint="Enter valid Email ID"/>
				</div>
				<div class="col-md-6">
					<label for="password">Password</label> <input type="password"
						class="form-control" name="password"
						placeholder="(ex: **********)"
						data-hint="Password should be minimun 6 characters" />
				</div>
			</div>
			<div class="row" style="padding-top: 10px">
				<div class="col-md-6">
					<label for="username">Username</label> <input type="text"
						class="form-control" name="username" placeholder="(ex: johns)"
						value="<?php echo isset($_POST['username']) ? $_POST['username']:'';?>" />
				</div>
				<div class="col-md-3">
					<label> Gender</label>
					<div class="radio">
						<label><input type="radio" name="gender" value="m"
							<?php if(isset($_POST['gender']) and !empty($_POST['gender']) == true){echo (($_POST['gender'] == "m")? 'checked': '');}?> />
							Male</label><label> <input type="radio" name="gender" value="f"
							<?php if(isset($_POST['gender']) and !empty($_POST['gender']) == true){echo (($_POST['gender'] == "f")? 'checked': '');}?> />
							Female
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="padding-top: 10px">
					<div class="checkbox">
						<label> <input name="terms_to_agree" value="1" type="checkbox"
							<?php
							if (isset ( $_POST ['terms_to_agree'] ) and ! empty ( $_POST ['terms_to_agree'] ) == true) {
								if ($_POST ['terms_to_agree'] == 1) {
									echo 'checked';
								}
							}
							?>> I agree to the<a href="terms_of_use.php" target="_blank">
								terms of use</a>
						</label>
					</div>
				</div>
			</div>
			<div class="row" style="padding-top: 10px">
				<div class="col-md-2 col-md-offset-5">
					<button type="submit" class="btn btn-primary ">Register</button>
				</div>
			</div>

		</form>
	</div>
</div>
<?php
require_once 'inc_footer.php';
?>
